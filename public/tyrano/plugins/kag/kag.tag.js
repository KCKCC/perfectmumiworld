tyrano.plugin.kag.ftag = {
    tyrano: null,
    kag: null,
    array_tag: [],
    master_tag: {},
    current_order_index: -1,
    init: function() {
        for (var a in tyrano.plugin.kag.tag) {
            this.master_tag[a] = object(tyrano.plugin.kag.tag[a]);
            this.master_tag[a].kag = this.kag;
        }
    },
    buildTag: function(a, b) {
        this.array_tag = a;
        if (b) this.nextOrderWithLabel(b); else this.nextOrderWithLabel("");
    },
    buildTagIndex: function(a, b, c) {
        this.array_tag = a;
        this.nextOrderWithIndex(b, void 0, void 0, void 0, c);
    },
    completeTrans: function() {
        this.kag.stat.is_trans = false;
        if (true == this.kag.stat.is_stop) {
            this.kag.layer.showEventLayer();
            this.nextOrder();
        }
    },
    hideNextImg: function() {
        $(".img_next").remove();
        $(".glyph_image").hide();
    },
    nextOrder: function() {
        this.kag.layer.layer_event.hide();
        var a = this;
        if (true == this.kag.stat.is_strong_stop) return false;
        try {
            this.current_order_index++;
            if (this.array_tag.length <= this.current_order_index) {
                this.kag.endStorage();
                return false;
            }
            var b = $.cloneObject(this.array_tag[this.current_order_index]);
            this.kag.stat.current_line = b.line;
            if (this.kag.is_rider) {
				console.log(445);
                b.ks_file = this.kag.stat.current_scenario;
                this.kag.rider.pushConsoleLog(b);
            } else {
                this.kag.log("**:" + this.current_order_index + "　line:" + b.line);
                this.kag.log(b);
            }
            if ("call" == b.name && "make.ks" == b.pm.storage || "make.ks" == this.kag.stat.current_scenario) ; else if (true == this.kag.stat.flag_ref_page) {
                this.kag.stat.flag_ref_page = false;
                this.kag.ftag.hideNextImg();
                this.kag.getMessageInnerLayer().html("");
            }
            if (true != this.checkCond(b)) {
                this.nextOrder();
                return;
            }
            if (true == this.kag.stat.is_hide_message) {
                this.kag.layer.showMessageLayers();
                this.kag.stat.is_hide_message = false;
            }
            if (this.master_tag[b.name]) {
                b.pm = this.convertEntity(b.pm);
                var c = this.checkVital(b);
                if (this.checkCw(b)) this.kag.layer.layer_event.show();
                if ("" != c) this.kag.error(c); else this.master_tag[b.name].start($.extend(true, $.cloneObject(this.master_tag[b.name].pm), b.pm));
            } else if (this.kag.stat.map_macro[b.name]) {
                b.pm = this.convertEntity(b.pm);
                var d = b.pm;
                var e = this.kag.stat.map_macro[b.name];
                var f = {};
                f.index = this.kag.ftag.current_order_index;
                f.storage = this.kag.stat.current_scenario;
                f.pm = d;
                this.kag.stat.mp = d;
                this.kag.pushStack("macro", f);
                this.kag.ftag.nextOrderWithIndex(e.index, e.storage);
            } else {
                $.error_message($.lang("tag") + "：[" + b.name + "]" + $.lang("not_exists"));
                this.nextOrder();
            }
        } catch (g) {
            console.log(g);
            a.kag.error($.lang("error_occurred"));
        }
    },
    checkCw: function(a) {
        var b = this.master_tag[a.name];
        if (b.cw) if (true != this.kag.stat.is_script && true != this.kag.stat.is_html && true != this.kag.stat.checking_macro) return true; else return false; else return false;
    },
    nextOrderWithTag: function(a) {
        try {
            this.current_order_index++;
            var b = this.array_tag[this.current_order_index];
            if (true != this.checkCond(b)) ;
            if ("" == a[b.name]) if (this.master_tag[b.name]) {
                b.pm = this.convertEntity(b.pm);
                this.master_tag[b.name].start($.extend(true, $.cloneObject(this.master_tag[b.name].pm), b.pm));
                return true;
            } else return false; else return false;
        } catch (c) {
            console.log(this.array_tag);
            return false;
        }
    },
    convertEntity: function(a) {
        var b = this;
        if ("" == a["*"]) a = $.extend(true, $.cloneObject(a), this.kag.stat.mp);
        for (key in a) {
            var c = a[key];
            var d = "";
            if (c.length > 0) d = c.substr(0, 1);
            if (c.length > 0 && "&" === d) a[key] = this.kag.embScript(c.substr(1, c.length)); else if (c.length > 0 && "%" === d) {
                var e = this.kag.getStack("macro");
                if (e) a[key] = e.pm[c.substr(1, c.length)];
                var f = c.split("|");
                if (2 == f.length) if (e.pm[$.trim(f[0]).substr(1, $.trim(f[0]).length)]) a[key] = e.pm[$.trim(f[0]).substr(1, $.trim(f[0]).length)]; else a[key] = $.trim(f[1]);
            }
        }
        return a;
    },
    checkVital: function(a) {
        var b = this.master_tag[a.name];
        var c = "";
        if (b.vital) ; else return "";
        var d = b.vital;
        for (var e = 0; e < d.length; e++) if (a.pm[d[e]]) {
            if ("" == a.pm[d[e]]) c += "タグ「" + a.name + "」にパラメーター「" + d[e] + "」は必須です　\n";
        } else c += "タグ「" + a.name + "」にパラメーター「" + d[e] + "」は必須です　\n";
        return c;
    },
    checkCond: function(a) {
        var b = a.pm;
        if (b.cond) {
            var c = b.cond;
            return this.kag.embScript(c);
        } else return true;
    },
    startTag: function(a, b) {
        this.master_tag[a].start($.extend(true, $.cloneObject(this.master_tag[a].pm), b));
    },
    nextOrderWithLabel: function(a, b) {
        this.kag.stat.is_strong_stop = false;
        if (a) {
            if (a.indexOf("*") != -1) a = a.substr(1, a.length);
            this.kag.ftag.startTag("label", {
                label_name: a,
                nextorder: "false"
            });
        }
        if ("*savesnap" == a) {
            var c = this.kag.menu.snap;
            var d = c.current_order_index;
            var e = c.stat.current_scenario;
            this.nextOrderWithIndex(d, e, void 0, void 0, "snap");
            return;
        }
        var f = this;
        var g = b;
        a = a || "";
        b = b || this.kag.stat.current_scenario;
        a = a.replace("*", "");
        if (b != this.kag.stat.current_scenario && null != g) {
            this.kag.layer.hideEventLayer();
			//kc don't know why it continue load ks again and again ;_;???
			this.kag.loadScenario(b, function(b) {
				f.kag.layer.showEventLayer();
				f.kag.ftag.buildTag(b, a);
			});
            return;
        }
        if ("" == a) {
            this.current_order_index = -1;
            this.nextOrder();
        } else if (this.kag.stat.map_label[a]) {
            var h = this.kag.stat.map_label[a];
            this.current_order_index = h.index;
            this.nextOrder();
        } else {
            $.error_message($.lang("label") + "：'" + a + "'" + $.lang("not_exists"));
            this.nextOrder();
        }
    },
    nextOrderWithIndex: function(a, b, c, d, e) {
        this.kag.stat.is_strong_stop = false;
        this.kag.layer.showEventLayer();
        var f = this;
        c = c || false;
        e = e || "yes";
        b = b || this.kag.stat.current_scenario;
        if (b != this.kag.stat.current_scenario || true == c) {
            this.kag.layer.hideEventLayer();

			this.kag.loadScenario(b, function(b) {
				if ("object" == typeof d) b.splice(a + 1, 0, d);
				f.kag.layer.showEventLayer();
				f.kag.ftag.buildTagIndex(b, a, e);
			});

            return;
        }
        this.current_order_index = a;
        if ("yes" == e) this.nextOrder(); else if ("snap" == e) {
            this.kag.stat.is_strong_stop = this.kag.menu.snap.stat.is_strong_stop;
            if (true == this.kag.stat.is_skip && false == this.kag.stat.is_strong_stop) this.kag.ftag.nextOrder();
        } else if ("stop" == e) this.kag.ftag.startTag("s", {
            val: {}
        });
    }
};

tyrano.plugin.kag.tag.text = {
    cw: true,
    pm: {
        val: ""
    },
    start: function(a) {
        if (true == this.kag.stat.is_script) {
            this.kag.stat.buff_script += a.val + "\n";
            this.kag.ftag.nextOrder();
            return;
        }
        if (true == this.kag.stat.is_html) {
            this.kag.stat.map_html.buff_html += a.val;
            this.kag.ftag.nextOrder();
            return;
        }
        var b = this.kag.getMessageInnerLayer();
        b.css({
            "letter-spacing": this.kag.config.defaultPitch + "px",
            "line-height": parseInt(this.kag.config.defaultFontSize) + parseInt(this.kag.config.defaultLineSpacing) + "px",
            "font-family": this.kag.config.userFace
        });
        this.kag.stat.current_message_str = a.val;
        if ("true" == this.kag.stat.vertical) {
            if ("false" != this.kag.config.defaultAutoReturn) {
                var c = this.kag.getMessageOuterLayer();
                var d = .8 * parseInt(c.css("width"));
                var e = parseInt(b.find("p").css("width"));
                if (e > d) this.kag.getMessageInnerLayer().html("");
            }
            this.showMessageVertical(a.val);
        } else {
            if ("false" != this.kag.config.defaultAutoReturn) {
                var c = this.kag.getMessageOuterLayer();
                var f = .8 * parseInt(c.css("height"));
                var g = parseInt(b.find("p").css("height"));
                if (g > f) this.kag.getMessageInnerLayer().html("");
            }
            this.showMessage(a.val);
        }
    },
    showMessage: function(a) {
        var b = this;
        var c = $.isNull($(".chara_name_area").html());
        if ("" != c) this.kag.pushBackLog("<b>" + c + "</b>：" + a); else this.kag.pushBackLog(a);
        b.kag.ftag.hideNextImg();
        !function(c) {
            if ("" == c.html()) c.append("<p class=''></p>");
            var d = a;
            var e = "";
            if (0 != c.find("p").find(".current_span").length) e = c.find("p").find(".current_span").html();
            var f = 0;
            b.kag.checkMessage(c);
            var g = b.kag.getMessageCurrentSpan();
            g.css({
                color: b.kag.stat.font.color,
                "font-weight": b.kag.stat.font.bold,
                "font-size": b.kag.stat.font.size + "px",
                "font-family": b.kag.stat.font.face,
                "font-style": b.kag.stat.font.italic
            });
            if ("true" == b.kag.config.autoRecordLabel) if (true == b.kag.stat.already_read) {
                if ("default" != b.kag.config.alreadyReadTextColor) g.css("color", $.convertColor(b.kag.config.alreadyReadTextColor));
            } else if ("false" == b.kag.config.unReadTextSkip) b.kag.stat.is_skip = false;
            var h = 30;
            if ("" != b.kag.stat.ch_speed) h = parseInt(b.kag.stat.ch_speed); else if (b.kag.config.chSpeed) h = parseInt(b.kag.config.chSpeed);
            var i = function(a) {
                var g = d.substring(f, ++f);
                if ("" != b.kag.stat.ruby_str) {
                    g = "<ruby><rb>" + g + "</rb><rt>" + b.kag.stat.ruby_str + "</rt></ruby>";
                    b.kag.stat.ruby_str = "";
                }
                e += g;
                if (true != b.kag.stat.is_skip && true != b.kag.stat.is_nowait) b.kag.appendMessage(c, e);
                if (f <= d.length) {
                    b.kag.stat.is_adding_text = true;
                    if (true == b.kag.stat.is_click_text || true == b.kag.stat.is_skip || true == b.kag.stat.is_nowait) setTimeout(function() {
                        a(a);
                    }, 0); else setTimeout(function() {
                        a(a);
                    }, h);
                } else {
                    b.kag.stat.is_adding_text = false;
                    b.kag.stat.is_click_text = false;
                    if ("true" != b.kag.stat.is_stop) {
                        if (true == b.kag.stat.is_skip || true == b.kag.stat.is_nowait) {
                            b.kag.appendMessage(c, e);
                            setTimeout(function() {
                                if (!b.kag.stat.is_hide_message) b.kag.ftag.nextOrder();
                            }, parseInt(b.kag.config.skipSpeed));
                        } else if (!b.kag.stat.is_hide_message) b.kag.ftag.nextOrder();
                    } else ;
                    if ("false" == b.kag.stat.flag_glyph) {
                        $(".img_next").remove();
                        c.find("p").append("<img class='img_next' src='./tyrano/images/kag/nextpage.gif' />");
                    } else $(".glyph_image").show();
                }
            };
            i(i);
        }(this.kag.getMessageInnerLayer());
    },
    showMessageVertical: function(a) {
        var b = this;
        b.kag.ftag.hideNextImg();
        this.kag.pushBackLog(a);
        !function(c) {
            if ("" == c.html()) c.append("<p class='vertical_text'></p>");
            var d = a;
            var e = "";
            if (0 != c.find("p").find(".current_span").length) e = c.find("p").find(".current_span").html();
            var f = 0;
            b.kag.checkMessage(c);
            var g = b.kag.getMessageCurrentSpan();
            g.css("color", b.kag.stat.font.color).css("font-weight", b.kag.stat.font.bold).css("font-size", b.kag.stat.font.size + "px").css("font-family", b.kag.stat.font.face);
            if ("true" == b.kag.config.autoRecordLabel) if ("default" != b.kag.config.alreadyReadTextColor) if (true == b.kag.stat.already_read) g.css("color", $.convertColor(b.kag.config.alreadyReadTextColor));
            var h = 30;
            if ("" != b.kag.stat.ch_speed) h = parseInt(b.kag.stat.ch_speed); else if (b.kag.config.chSpeed) h = parseInt(b.kag.config.chSpeed);
            var i = function(a) {
                var g = d.substring(f, ++f);
                if ("" != b.kag.stat.ruby_str) {
                    g = "<ruby><rb>" + g + "</rb><rt>" + b.kag.stat.ruby_str + "</rt></ruby>";
                    b.kag.stat.ruby_str = "";
                }
                e += g;
                if (true != b.kag.stat.is_skip && true != b.kag.stat.is_nowait) b.kag.appendMessage(c, e);
                if (f <= d.length) {
                    b.kag.stat.is_adding_text = true;
                    if (true == b.kag.stat.is_click_text || true == b.kag.stat.is_skip) setTimeout(function() {
                        a(a);
                    }, 0); else setTimeout(function() {
                        a(a);
                    }, h);
                } else {
                    b.kag.stat.is_adding_text = false;
                    b.kag.stat.is_click_text = false;
                    if (true == b.kag.stat.is_skip || true == b.kag.stat.is_nowait) {
                        b.kag.appendMessage(c, e);
                        setTimeout(function() {
                            b.kag.ftag.nextOrder();
                        }, parseInt(b.kag.config.skipSpeed));
                    } else b.kag.ftag.nextOrder();
                    if ("false" == b.kag.stat.flag_glyph) {
                        $(".img_next").remove();
                        c.find("p").append("<img class='img_next' src='./tyrano/images/kag/nextpage.gif' />");
                    } else $(".glyph_image").show();
                }
            };
            i(i);
        }(this.kag.getMessageInnerLayer());
    },
    nextOrder: function() {},
    test: function() {}
};

tyrano.plugin.kag.tag.label = {
    pm: {
        nextorder: "true"
    },
    start: function(a) {
        if ("true" == this.kag.config.autoRecordLabel) {
            var b = "trail_" + this.kag.stat.current_scenario.replace(".ks", "").replace(/\u002f/g, "").replace(/:/g, "").replace(/\./g, "");
            var c = this.kag.stat.buff_label_name;
            var d = b + "_" + a.label_name;
            if ("" != this.kag.stat.buff_label_name) {
                if (!this.kag.variable.sf.record) this.kag.variable.sf.record = {};
                var e = "sf.record." + c;
                var f = "" + e + " = " + e + "  || 0;" + e + "++;";
                this.kag.evalScript(f);
            }
            if (this.kag.variable.sf.record) if (this.kag.variable.sf.record[d]) this.kag.stat.already_read = true; else this.kag.stat.already_read = false;
            this.kag.stat.buff_label_name = d;
        }
        if ("true" == a.nextorder) this.kag.ftag.nextOrder();
    }
};

tyrano.plugin.kag.tag.config_record_label = {
    pm: {
        color: "",
        skip: ""
    },
    start: function(a) {
        var b = this;
        if ("" != a.color) {
            this.kag.config.alreadyReadTextColor = a.color;
            this.kag.ftag.startTag("eval", {
                exp: "sf._system_config_already_read_text_color = " + a.color
            });
        }
        if ("" != a.skip) {
            this.kag.config.unReadTextSkip = a.skip;
            this.kag.ftag.startTag("eval", {
                exp: "sf._system_config_unread_text_skip = " + a.skip
            });
        }
        this.kag.ftag.nextOrder();
    }
};

tyrano.plugin.kag.tag.l = {
    cw: true,
    start: function() {
        var a = this;
        if (true == this.kag.stat.is_skip) this.kag.ftag.nextOrder(); else if (true == this.kag.stat.is_auto) {
            this.kag.stat.is_wait_auto = true;
            setTimeout(function() {
                if (true == a.kag.stat.is_wait_auto) a.kag.ftag.nextOrder();
            }, parseInt(a.kag.config.autoSpeed));
        }
    }
};

tyrano.plugin.kag.tag.p = {
    cw: true,
    start: function() {
        var a = this;
        this.kag.stat.flag_ref_page = true;
        if (true == this.kag.stat.is_skip) this.kag.ftag.nextOrder(); else if (true == this.kag.stat.is_auto) {
            this.kag.stat.is_wait_auto = true;
            setTimeout(function() {
                if (true == a.kag.stat.is_wait_auto) a.kag.ftag.nextOrder();
            }, parseInt(a.kag.config.autoSpeed));
        }
    }
};

tyrano.plugin.kag.tag.graph = {
    vital: [ "storage" ],
    pm: {
        storage: null
    },
    start: function(a) {
        var b = this.kag.getMessageInnerLayer();
        var c = "";
        if (0 != b.find("p").find(".current_span").length) c = b.find("p").find(".current_span").html();
        var d = "";
        if ($.isHTTP(a.storage)) d = a.storage; else d = "./data/image/" + a.storage;
        this.kag.appendMessage(b, c + "<img src='" + d + "' >");
        this.kag.ftag.nextOrder();
    }
};

tyrano.plugin.kag.tag.jump = {
    pm: {
        storage: null,
        target: null,
        countpage: true
    },
    start: function(a) {
        this.kag.ftag.nextOrderWithLabel(a.target, a.storage);
    }
};

tyrano.plugin.kag.tag.r = {
    start: function() {
        var a = this.kag.getMessageInnerLayer();
        var b = a.find("p").find(".current_span").html() + "<br />";
        a.find("p").find(".current_span").html(b);
        this.kag.ftag.nextOrder();
    }
};

tyrano.plugin.kag.tag.er = {
    start: function() {
        this.kag.ftag.hideNextImg();
        this.kag.getMessageInnerLayer().html("");
        this.kag.ftag.startTag("resetfont");
    }
};

tyrano.plugin.kag.tag.cm = {
    start: function() {
        this.kag.ftag.hideNextImg();
        this.kag.layer.clearMessageInnerLayerAll();
        this.kag.layer.getFreeLayer().html("").hide();
        this.kag.ftag.startTag("resetfont");
    }
};

tyrano.plugin.kag.tag.ct = {
    start: function() {
        this.kag.ftag.hideNextImg();
        this.kag.layer.clearMessageInnerLayerAll();
        this.kag.layer.getFreeLayer().html("").hide();
        this.kag.stat.current_layer = "message0";
        this.kag.stat.current_page = "fore";
        this.kag.ftag.startTag("resetfont");
    }
};

tyrano.plugin.kag.tag.current = {
    pm: {
        layer: "",
        page: "fore"
    },
    start: function(a) {
        if ("" == a.layer) a.layer = this.kag.stat.current_layer;
        this.kag.stat.current_layer = a.layer;
        this.kag.stat.current_page = a.page;
        this.kag.ftag.nextOrder();
    }
};

tyrano.plugin.kag.tag.position = {
    pm: {
        layer: "message0",
        page: "fore",
        left: "",
        top: "",
        width: "",
        height: "",
        color: "",
        opacity: "",
        vertical: "",
        frame: "",
        marginl: "0",
        margint: "0",
        marginr: "0",
        marginb: "0"
    },
    start: function(a) {
        var b = this.kag.layer.getLayer(a.layer, a.page).find(".message_outer");
        var c = {
            left: a.left + "px",
            top: a.top + "px",
            width: a.width + "px",
            height: a.height + "px",
            "background-color": $.convertColor(a.color)
        };
        if ("" != a.vertical) if ("true" == a.vertical) this.kag.stat.vertical = "true"; else this.kag.stat.vertical = "false";
        if ("none" == a.frame) {
            b.css("opacity", $.convertOpacity(this.kag.config.frameOpacity));
            b.css("background-image", "");
            b.css("background-color", $.convertColor(this.kag.config.frameColor));
        } else if ("" != a.frame) {
            var d = "";
            if ($.isHTTP(a.frame)) d = a.frame; else d = "./data/image/" + a.frame;
            b.css("background-image", "url(" + d + ")");
            b.css("background-repeat", "no-repeat");
            b.css("opacity", 1);
            b.css("background-color", "");
        }
        if ("" != a.opacity) b.css("opacity", $.convertOpacity(a.opacity));
        this.kag.setStyles(b, c);
        this.kag.layer.refMessageLayer();
        var e = this.kag.layer.getLayer(a.layer, a.page).find(".message_inner");
        var f = {};
        if ("0" != a.marginl) f["padding-left"] = parseInt(a.marginl) + "px";
        if ("0" != a.margint) f["padding-top"] = parseInt(a.margint) + "px";
        if ("0" != a.marginr) f["width"] = parseInt(e.css("width")) - parseInt(a.marginr) + "px";
        if ("0" != a.marginb) f["height"] = parseInt(e.css("height")) - parseInt(a.marginb) + "px";
        this.kag.setStyles(e, f);
        this.kag.ftag.nextOrder();
    }
};

tyrano.plugin.kag.tag.image = {
    pm: {
        layer: "base",
        page: "fore",
        visible: "",
        top: "",
        left: "",
        x: "",
        y: "",
        width: "",
        height: "",
        pos: "",
        name: "",
        folder: "",
        time: ""
    },
    start: function(a) {
        var b = "";
        var c = "";
        var d = this;
        if ("base" != a.layer) {
            var e = {};
            if ("true" == a.visible && "fore" == a.page) e.display = "block";
            this.kag.setStyles(this.kag.layer.getLayer(a.layer, a.page), e);
            if ("" != a.pos) switch (a.pos) {
              case "left":
              case "l":
                a.left = this.kag.config["scPositionX.left"];
                break;

              case "left_center":
              case "lc":
                a.left = this.kag.config["scPositionX.left_center"];
                break;

              case "center":
              case "c":
                a.left = this.kag.config["scPositionX.center"];
                break;

              case "right_center":
              case "rc":
                a.left = this.kag.config["scPositionX.right_center"];
                break;

              case "right":
              case "r":
                a.left = this.kag.config["scPositionX.right"];
            }
            if ("" != a.folder) c = a.folder; else c = "fgimage";
            if ($.isHTTP(a.storage)) b = a.storage; else b = "./data/" + c + "/" + a.storage;
            var f = $("<img />");
            f.attr("src", b);
            f.css("position", "absolute");
            f.css("top", a.top + "px");
            f.css("left", a.left + "px");
            if ("" != a.width) f.css("width", a.width + "px");
            if ("" != a.height) f.css("height", a.height + "px");
            if ("" != a.x) f.css("left", a.x + "px");
            if ("" != a.y) f.css("top", a.y + "px");
            $.setName(f, a.name);
            if (0 == a.time) a.time = "";
            if ("" != a.time) {
                f.css("opacity", 0);
                this.kag.layer.getLayer(a.layer, a.page).append(f);
                f.animate({
                    opacity: 1
                }, parseInt(a.time), function() {
                    d.kag.ftag.nextOrder();
                });
            } else {
                this.kag.layer.getLayer(a.layer, a.page).append(f);
                this.kag.ftag.nextOrder();
            }
        } else {
            if ("" != a.folder) c = a.folder; else c = "bgimage";
            if ($.isHTTP(a.storage)) b = a.storage; else b = "./data/" + c + "/" + a.storage;
            var g = {
                "background-image": "url(" + b + ")",
                display: "none"
            };
            if ("fore" === a.page) g.display = "block";
            this.kag.setStyles(this.kag.layer.getLayer(a.layer, a.page), g);
            this.kag.ftag.nextOrder();
        }
    }
};

tyrano.plugin.kag.tag.freeimage = {
    vital: [ "layer" ],
    pm: {
        layer: "",
        page: "fore",
        time: ""
    },
    start: function(a) {
        var b = this;
        if ("base" != a.layer) {
            if (0 == a.time) a.time = "";
            if ("" != a.time) {
                var c = this.kag.layer.getLayer(a.layer, a.page).children();
                var d = 0;
                var e = c.length;
                c.animate({
                    opacity: 0
                }, parseInt(a.time), function() {
                    b.kag.layer.getLayer(a.layer, a.page).empty();
                    d++;
                    if (e == d) b.kag.ftag.nextOrder();
                });
            } else {
                b.kag.layer.getLayer(a.layer, a.page).empty();
                b.kag.ftag.nextOrder();
            }
        } else {
            this.kag.layer.getLayer(a.layer, a.page).css("background-image", "");
            this.kag.ftag.nextOrder();
        }
    }
};

tyrano.plugin.kag.tag.freelayer = tyrano.plugin.kag.tag.freeimage;

tyrano.plugin.kag.tag.free = {
    vital: [ "layer", "name" ],
    pm: {
        layer: "",
        page: "fore",
        name: "",
        wait: "true",
        time: ""
    },
    start: function(a) {
        var b = this;
        if ("base" != a.layer) {
            if (0 == a.time) a.time = "";
            if ("" != a.time) {
                var c = this.kag.layer.getLayer(a.layer, a.page);
                c = c.find("." + a.name);
                var d = 0;
                var e = c.length;
                c.animate({
                    opacity: 0
                }, parseInt(a.time), function() {
                    c.remove();
                    d++;
                    if (d == e) if ("true" == a.wait) b.kag.ftag.nextOrder();
                });
                if ("false" == a.wait) b.kag.ftag.nextOrder();
            } else {
                var c = this.kag.layer.getLayer(a.layer, a.page);
                c = c.find("." + a.name);
                c.remove();
                b.kag.ftag.nextOrder();
            }
        } else {
            var c = this.kag.layer.getLayer(a.layer, a.page);
            c = c.find("." + a.name);
            c.remove();
            this.kag.ftag.nextOrder();
        }
    }
};

tyrano.plugin.kag.tag.ptext = {
    vital: [ "layer", "x", "y" ],
    pm: {
        layer: "0",
        page: "fore",
        x: 0,
        y: 0,
        vertical: "false",
        text: "",
        size: "",
        face: "",
        color: "",
        italic: "",
        bold: "",
        name: "",
        time: "",
        zindex: "9999",
        overwrite: "false"
    },
    start: function(a) {
        var b = this;
        if ("" == a.face) a.face = b.kag.stat.font.face;
        if ("" == a.color) a.color = $.convertColor(b.kag.stat.font.color); else a.color = $.convertColor(a.color);
        var c = {
            color: a.color,
            "font-weight": a.bold,
            "font-style": a.fontstyle,
            "font-size": a.size + "px",
            "font-family": a.face,
            "z-index": "999",
            text: ""
        };
        var d = this.kag.layer.getLayer(a.layer, a.page);
        if ("true" == a.overwrite && "" != a.name) if ($("." + a.name).size() > 0) {
            $("." + a.name).html(a.text);
            this.kag.ftag.nextOrder();
            return false;
        }
        var e = $("<p></p>");
        e.css("position", "absolute");
        e.css("top", a.y + "px");
        e.css("left", a.x + "px");
        e.css("width", "100%");
        if ("true" == a.vertical) e.addClass("vertical_text");
        $.setName(e, a.name);
        e.html(a.text);
        this.kag.setStyles(e, c);
        if ("fix" == a.layer) e.addClass("fixlayer");
        if ("" != a.time) {
            e.css("opacity", 0);
            d.append(e);
            e.animate({
                opacity: 1
            }, parseInt(a.time), function() {
                b.kag.ftag.nextOrder();
            });
        } else {
            this.kag.ftag.nextOrder();
            d.append(e);
        }
    }
};

tyrano.plugin.kag.tag.mtext = {
    vital: [ "x", "y" ],
    pm: {
        layer: "0",
        page: "fore",
        x: 0,
        y: 0,
        vertical: "false",
        text: "",
        size: "",
        face: "",
        color: "",
        italic: "",
        bold: "",
        name: "",
        zindex: "9999",
        fadeout: "true",
        time: "2000",
        in_effect: "fadeIn",
        in_delay: "50",
        in_delay_scale: "1.5",
        in_sync: "false",
        in_shuffle: "false",
        in_reverse: "false",
        wait: "true",
        out_effect: "fadeOut",
        out_delay: "50",
        out_scale_delay: "",
        out_sync: "false",
        out_shuffle: "false",
        out_reverse: "false"
    },
    start: function(a) {
        var b = this;
        if ("" == a.face) a.face = b.kag.stat.font.face;
        if ("" == a.color) a.color = $.convertColor(b.kag.stat.font.color); else a.color = $.convertColor(a.color);
        var c = {
            color: a.color,
            "font-weight": a.bold,
            "font-style": a.fontstyle,
            "font-size": a.size + "px",
            "font-family": a.face,
            "z-index": "999",
            text: ""
        };
        var d = this.kag.layer.getLayer(a.layer, a.page);
        var e = $("<p></p>");
        e.css("position", "absolute");
        e.css("top", a.y + "px");
        e.css("left", a.x + "px");
        e.css("width", "100%");
        if ("true" == a.vertical) e.addClass("vertical_text");
        $.setName(e, a.name);
        e.html(a.text);
        this.kag.setStyles(e, c);
        if ("fix" == a.layer) e.addClass("fixlayer");
        d.append(e);
        for (key in a) if ("true" == a[key]) a[key] = true; else if ("false" == a[key]) a[key] = false;
        e.textillate({
            loop: a["fadeout"],
            minDisplayTime: a["time"],
            "in": {
                effect: a["in_effect"],
                delayScale: a["in_delay_scale"],
                delay: a["in_delay"],
                sync: a["in_sync"],
                shuffle: a["in_shuffle"],
                reverse: a["in_reverse"],
                callback: function() {
                    if (false == a.fadeout && true == a.wait) b.kag.ftag.nextOrder();
                }
            },
            out: {
                effect: a["out_effect"],
                delayScale: a["out_delay_scale"],
                delay: a["out_delay"],
                sync: a["out_sync"],
                shuffle: a["out_shuffle"],
                reverse: a["out_reverse"],
                callback: function() {
                    e.remove();
                    if (true == a.wait) b.kag.ftag.nextOrder();
                }
            }
        });
        if (true != a.wait) this.kag.ftag.nextOrder();
    }
};

tyrano.plugin.kag.tag.backlay = {
    pm: {
        layer: ""
    },
    start: function(a) {
        this.kag.layer.backlay(a.layer);
        this.kag.ftag.nextOrder();
    }
};

tyrano.plugin.kag.tag.wt = {
    start: function(a) {
        if (false == this.kag.stat.is_trans) {
            this.kag.layer.showEventLayer();
            this.kag.ftag.nextOrder();
        } else this.kag.layer.hideEventLayer();
    }
};

tyrano.plugin.kag.tag.wb = {
    start: function(a) {
        this.kag.layer.hideEventLayer();
    }
};

tyrano.plugin.kag.tag.link = {
    pm: {
        target: null,
        storage: null
    },
    start: function(a) {
        var b = this;
        var c = this.kag.setMessageCurrentSpan();
        c.css("cursor", "pointer");
        !function() {
            var d = a.target;
            var e = a.storage;
            b.kag.event.addEventElement({
                tag: "link",
                j_target: c,
                pm: a
            });
            b.setEvent(c, a);
        }();
        this.kag.ftag.nextOrder();
    },
    setEvent: function(a, b) {
        var c = b.target;
        var d = b.storage;
        var e = this;
        a.bind("click", function(a) {
            TYRANO.kag.ftag.nextOrderWithLabel(c, d);
            TYRANO.kag.layer.showEventLayer();
            if ("true" == e.kag.stat.skip_link) a.stopPropagation(); else e.kag.stat.is_skip = false;
        });
        a.css("cursor", "pointer");
    }
};

tyrano.plugin.kag.tag.endlink = {
    start: function(a) {
        var b = this.kag.setMessageCurrentSpan();
        this.kag.ftag.nextOrder();
    }
};

tyrano.plugin.kag.tag.s = {
    start: function() {
        this.kag.stat.is_strong_stop = true;
        this.kag.layer.hideEventLayer();
    }
};

tyrano.plugin.kag.tag._s = {
    vital: [],
    pm: {},
    start: function(a) {
        this.kag.stat.strong_stop_recover_index = this.kag.ftag.current_order_index;
        this.kag.ftag.nextOrder();
    }
};

tyrano.plugin.kag.tag.wait = {
    vital: [ "time" ],
    pm: {
        time: 0
    },
    start: function(a) {
        var b = this;
        this.kag.stat.is_strong_stop = true;
        this.kag.layer.hideEventLayer();
        setTimeout(function() {
            b.kag.stat.is_strong_stop = false;
            b.kag.layer.showEventLayer();
            b.kag.ftag.nextOrder();
        }, a.time);
    }
};

tyrano.plugin.kag.tag.hidemessage = {
    start: function() {
        this.kag.stat.is_hide_message = true;
        this.kag.layer.hideMessageLayers();
        this.kag.layer.layer_event.show();
    }
};

tyrano.plugin.kag.tag.quake = {
    vital: [ "time" ],
    pm: {
        count: 5,
        time: 300,
        timemode: "",
        hmax: "0",
        vmax: "10",
        wait: "true"
    },
    start: function(a) {
        var b = this;
        if ("0" != a.hmax) $("." + this.kag.define.BASE_DIV_NAME).effect("shake", {
            times: parseInt(a.count),
            distance: parseInt(a.hmax),
            direction: "left"
        }, parseInt(a.time), function() {
            if ("true" == a.wait) b.kag.ftag.nextOrder();
        }); else if ("0" != a.vmax) $("." + this.kag.define.BASE_DIV_NAME).effect("shake", {
            times: parseInt(a.count),
            distance: parseInt(a.vmax),
            direction: "up"
        }, parseInt(a.time), function() {
            if ("true" == a.wait) b.kag.ftag.nextOrder();
        });
        if ("false" == a.wait) b.kag.ftag.nextOrder();
    }
};

tyrano.plugin.kag.tag.font = {
    pm: {},
    start: function(a) {
        this.kag.setMessageCurrentSpan();
        var b = {};
        if (a.size) this.kag.stat.font.size = a.size;
        if (a.color) this.kag.stat.font.color = $.convertColor(a.color);
        if (a.bold) this.kag.stat.font.bold = $.convertBold(a.bold);
        if (a.face) this.kag.stat.font.face = a.face;
        if (a.italic) this.kag.stat.font["italic"] = $.convertItalic(a.italic);
        this.kag.ftag.nextOrder();
    }
};

tyrano.plugin.kag.tag.deffont = {
    pm: {},
    start: function(a) {
        var b = {};
        if (a.size) this.kag.stat.default_font.size = a.size;
        if (a.color) this.kag.stat.default_font.color = $.convertColor(a.color);
        if (a.bold) this.kag.stat.default_font.bold = $.convertBold(a.bold);
        if (a.face) this.kag.stat.default_font.face = a.face;
        if (a.italic) this.kag.stat.default_font.italic = $.convertItalic(a.italic);
        this.kag.ftag.nextOrder();
    }
};

tyrano.plugin.kag.tag.delay = {
    pm: {
        speed: ""
    },
    start: function(a) {
        if ("" != a.speed) this.kag.stat.ch_speed = parseInt(a.speed);
        this.kag.ftag.nextOrder();
    }
};

tyrano.plugin.kag.tag.resetdelay = {
    pm: {
        speed: ""
    },
    start: function(a) {
        this.kag.stat.ch_speed = "";
        this.kag.ftag.nextOrder();
    }
};

tyrano.plugin.kag.tag.configdelay = {
    pm: {
        speed: ""
    },
    start: function(a) {
        if ("" != a.speed) {
            this.kag.stat.ch_speed = "";
            this.kag.config.chSpeed = a.speed;
            this.kag.ftag.startTag("eval", {
                exp: "sf._config_ch_speed = " + a.speed
            });
        }
        this.kag.ftag.nextOrder();
    }
};

tyrano.plugin.kag.tag.nowait = {
    pm: {},
    start: function(a) {
        this.kag.stat.is_nowait = true;
        this.kag.ftag.nextOrder();
    }
};

tyrano.plugin.kag.tag.endnowait = {
    pm: {},
    start: function(a) {
        this.kag.stat.is_nowait = false;
        this.kag.ftag.nextOrder();
    }
};

tyrano.plugin.kag.tag.resetfont = {
    start: function() {
        var a = this.kag.setMessageCurrentSpan();
        this.kag.stat.font = $.extend(true, {}, this.kag.stat.default_font);
        this.kag.ftag.nextOrder();
    }
};

tyrano.plugin.kag.tag.layopt = {
    vital: [ "layer" ],
    pm: {
        layer: "",
        page: "fore",
        visible: "",
        left: "",
        top: "",
        opacity: "",
        autohide: false,
        index: 10
    },
    start: function(a) {
        var b = this;
        if ("message" == a.layer) {
            a.layer = this.kag.stat.current_layer;
            a.page = this.kag.stat.current_page;
        }
        var c = this.kag.layer.getLayer(a.layer, a.page);
        if ("fix" == a.layer || "fixlayer" == a.layer) c = $("#tyrano_base").find(".fixlayer");
        if ("" != a.visible) if ("true" == a.visible) {
            if ("fore" == a.page) c.css("display", "");
            c.attr("l_visible", "true");
        } else {
            c.css("display", "none");
            c.attr("l_visible", "false");
        }
        if ("" != a.left) c.css("left", parseInt(a.left));
        if ("" != a.top) c.css("top", parseInt(a.top));
        if ("" != a.opacity) c.css("opacity", $.convertOpacity(a.opacity));
        this.kag.ftag.nextOrder();
    }
};

tyrano.plugin.kag.tag["ruby"] = {
    vital: [ "text" ],
    pm: {
        text: ""
    },
    start: function(a) {
        var b = a.text;
        this.kag.stat.ruby_str = b;
        this.kag.ftag.nextOrder();
    }
};

tyrano.plugin.kag.tag.cancelskip = {
    start: function(a) {
        this.kag.stat.is_skip = false;
        this.kag.ftag.nextOrder();
    }
};

tyrano.plugin.kag.tag.locate = {
    pm: {
        x: null,
        y: null
    },
    start: function(a) {
        if (null != a.x) this.kag.stat.locate.x = a.x;
        if (null != a.y) this.kag.stat.locate.y = a.y;
        this.kag.ftag.nextOrder();
    }
};

tyrano.plugin.kag.tag.button = {
    pm: {
        graphic: "",
        storage: null,
        target: null,
        ext: "",
        name: "",
        x: "",
        y: "",
        width: "",
        height: "",
        fix: "false",
        savesnap: "false",
        folder: "image",
        exp: "",
        prevar: "",
        visible: "true",
        hint: "",
        clickse: "",
        enterse: "",
        leavese: "",
        clickimg: "",
        enterimg: "",
        auto_next: "yes",
        role: ""
    },
    start: function(a) {
        var b = this;
        var c = null;
        if ("" != a.role) a.fix = "true";
        if ("false" == a.fix) {
            c = this.kag.layer.getFreeLayer();
            c.css("z-index", 999999);
        } else c = this.kag.layer.getLayer("fix");
        var d = "";
        if ($.isHTTP(a.graphic)) d = a.graphic; else d = "./data/" + a.folder + "/" + a.graphic;
        var e = $("<img />");
        e.attr("src", d);
        e.css("position", "absolute");
        e.css("cursor", "pointer");
        e.css("z-index", 99999999);
        if ("true" == a.visible) e.show(); else e.hide();
        if ("" == a.x) e.css("left", this.kag.stat.locate.x + "px"); else e.css("left", a.x + "px");
        if ("" == a.y) e.css("top", this.kag.stat.locate.y + "px"); else e.css("top", a.y + "px");
        if ("false" != a.fix) e.addClass("fixlayer");
        if ("" != a.width) e.css("width", a.width + "px");
        if ("" != a.height) e.css("height", a.height + "px");
        if ("" != a.hint) e.attr({
            title: a.hint,
            alt: a.hint
        });
        $.setName(e, a.name);
        b.kag.event.addEventElement({
            tag: "button",
            j_target: e,
            pm: a
        });
        b.setEvent(e, a);
        c.append(e);
        if ("false" == a.fix) c.show();
        this.kag.ftag.nextOrder();
    },
    setEvent: function(a, b) {
        var c = TYRANO;
        !function() {
            var d = b.target;
            var e = b.storage;
            var f = b;
            var g = c.kag.embScript(b.preexp);
            var h = false;
            a.hover(function() {
                if ("" != f.enterse) c.kag.ftag.startTag("playse", {
                    storage: f.enterse,
                    stop: true
                });
                if ("" != f.enterimg) {
                    var a = "";
                    if ($.isHTTP(f.enterimg)) a = f.enterimg; else a = "./data/" + f.folder + "/" + f.enterimg;
                    $(this).attr("src", a);
                }
            }, function() {
                if ("" != f.leavese) c.kag.ftag.startTag("playse", {
                    storage: f.leavese,
                    stop: true
                });
                if ("" != f.enterimg) {
                    var a = "";
                    if ($.isHTTP(f.graphic)) a = f.graphic; else a = "./data/" + f.folder + "/" + f.graphic;
                    $(this).attr("src", a);
                }
            });
            a.click(function(b) {
                if ("" != f.clickse) c.kag.ftag.startTag("playse", {
                    storage: f.clickse,
                    stop: true
                });
                if ("" != f.clickimg) {
                    var d = "";
                    if ($.isHTTP(f.clickimg)) d = f.clickimg; else d = "./data/" + f.folder + "/" + f.clickimg;
                    a.attr("src", d);
                }
                if (true == h && "false" == f.fix) return false;
                if (true != c.kag.stat.is_strong_stop && "false" == f.fix) return false;
                h = true;
                if ("" != f.exp) c.kag.embScript(f.exp, g);
                if ("true" == f.savesnap) {
                    if (true == c.kag.stat.is_stop) return false;
                    c.kag.menu.snapSave(c.kag.stat.current_message_str);
                }
                if ("none" == c.kag.layer.layer_event.css("display") && true != c.kag.stat.is_strong_stop) return false;
                if ("" != f.role) {
                    c.kag.stat.is_skip = false;
                    if ("auto" != f.role) c.kag.ftag.startTag("autostop", {});
                    if ("save" == f.role || "menu" == f.role || "quicksave" == f.role || "sleepgame" == f.role) if (true == c.kag.stat.is_adding_text) return false;
                    switch (f.role) {
                      case "save":
                        c.kag.menu.displaySave();
                        break;

                      case "load":
                        c.kag.menu.displayLoad();
                        break;

                      case "window":
                        c.kag.layer.hideMessageLayers();
                        break;

                      case "title":
                        $.confirm($.lang("go_title"), function() {
                            location.reload();
                        }, function() {
                            return false;
                        });
                        break;

                      case "menu":
                        c.kag.menu.showMenu();
                        break;

                      case "skip":
                        c.kag.ftag.startTag("skipstart", {});
                        break;

                      case "backlog":
                        c.kag.menu.displayLog();
                        break;

                      case "fullscreen":
                        c.kag.menu.screenFull();
                        break;

                      case "quicksave":
                        c.kag.menu.setQuickSave();
                        break;

                      case "quickload":
                        c.kag.menu.loadQuickSave();
                        break;

                      case "auto":
                        if (true == c.kag.stat.is_auto) c.kag.ftag.startTag("autostop", {}); else c.kag.ftag.startTag("autostart", {});
                        break;

                      case "sleepgame":
                        if (null != c.kag.tmp.sleep_game) return false;
                        c.kag.tmp.sleep_game = {};
                        c.kag.ftag.startTag("sleepgame", f);
                    }
                    b.stopPropagation();
                    return false;
                }
                c.kag.layer.showEventLayer();
                if ("" == f.role && "true" == f.fix) {
                    var e = c.kag.getStack("call");
                    if (null == e) {
                        if (true == c.kag.stat.is_strong_stop) f.auto_next = "stop"; else ;
                        c.kag.ftag.startTag("call", f);
                    } else {
                        c.kag.log("callスタックが残っている場合、fixボタンは反応しません");
                        c.kag.log(e);
                        return false;
                    }
                } else c.kag.ftag.startTag("jump", f);
                if ("true" == c.kag.stat.skip_link) b.stopPropagation(); else c.kag.stat.is_skip = false;
            });
        }();
    }
};

tyrano.plugin.kag.tag.glink = {
    pm: {
        color: "black",
        font_color: "0xffffff",
        storage: null,
        target: null,
        name: "",
        text: "",
        x: "auto",
        y: "",
        width: "",
        height: "",
        size: 30,
        graphic: "",
        enterimg: "",
        clickse: "",
        enterse: "",
        leavese: "",
        face: ""
    },
    start: function(a) {
        var b = this;
        var c = null;
        c = this.kag.layer.getFreeLayer();
        c.css("z-index", 999999);
        var d = $("<div class='glink_button'>" + a.text + "</div>");
        d.css("position", "absolute");
        d.css("cursor", "pointer");
        d.css("z-index", 99999999);
        d.css("font-size", a.size + "px");
        d.css("color", $.convertColor(a.font_color));
        if ("" != a.height) d.css("height", a.height + "px");
        if ("" != a.width) d.css("width", a.width + "px");
        if ("" != a.graphic) {
            d.removeClass("glink_button").addClass("button_graphic");
            var e = "./data/image/" + a.graphic;
            d.css("background-image", "url(" + e + ")");
            d.css("background-repeat", "no-repeat");
            d.css("background-position", "center center");
            d.css("background-size", "100% 100%");
        } else d.addClass(a.color);
        if ("" != a.face) d.css("font-family", a.face); else if ("" != b.kag.stat.font.face) d.css("font-family", b.kag.stat.font.face);
        if ("auto" == a.x) {
            var f = parseInt(b.kag.config.scWidth);
            var g = Math.floor(parseInt(d.css("width")) / 2);
            var h = Math.floor(f / 2);
            var i = h - g;
            d.css("left", i + "px");
        } else if ("" == a.x) d.css("left", this.kag.stat.locate.x + "px"); else d.css("left", a.x + "px");
        if ("" == a.y) d.css("top", this.kag.stat.locate.y + "px"); else d.css("top", a.y + "px");
        $.setName(d, a.name);
        b.kag.event.addEventElement({
            tag: "glink",
            j_target: d,
            pm: a
        });
        this.setEvent(d, a);
        c.append(d);
        c.show();
        this.kag.ftag.nextOrder();
    },
    setEvent: function(a, b) {
        var c = TYRANO;
        !function() {
            var d = b.target;
            var e = b.storage;
            var f = b;
            var g = c.kag.embScript(b.preexp);
            var h = false;
            a.click(function(a) {
                if ("" != f.clickse) c.kag.ftag.startTag("playse", {
                    storage: f.clickse
                });
                if (true != c.kag.stat.is_strong_stop) return false;
                h = true;
                if ("" != f.exp) c.kag.embScript(f.exp, g);
                c.kag.layer.showEventLayer();
                c.kag.ftag.startTag("cm", {});
                c.kag.ftag.startTag("jump", f);
                if ("true" == c.kag.stat.skip_link) a.stopPropagation(); else c.kag.stat.is_skip = false;
            });
            a.hover(function() {
                if ("" != f.enterimg) {
                    var b = "./data/image/" + f.enterimg;
                    a.css("background-image", "url(" + b + ")");
                }
                if ("" != f.enterse) c.kag.ftag.startTag("playse", {
                    storage: f.enterse,
                    stop: true
                });
            }, function() {
                if ("" != f.enterimg) {
                    var b = "./data/image/" + f.graphic;
                    a.css("background-image", "url(" + b + ")");
                }
                if ("" != f.leavese) c.kag.ftag.startTag("playse", {
                    storage: f.leavese,
                    stop: true
                });
            });
        }();
    }
};

tyrano.plugin.kag.tag.clickable = {
    vital: [ "width", "height" ],
    pm: {
        width: "0",
        height: "0",
        x: "",
        y: "",
        border: "none",
        color: "",
        mouseopacity: "",
        opacity: "140",
        storage: null,
        target: null,
        name: ""
    },
    start: function(a) {
        var b = this;
        var c = this.kag.layer.getFreeLayer();
        c.css("z-index", 9999999);
        var d = $("<div />");
        d.css("position", "absolute");
        d.css("cursor", "pointer");
        d.css("top", this.kag.stat.locate.y + "px");
        d.css("left", this.kag.stat.locate.x + "px");
        d.css("width", a.width + "px");
        d.css("height", a.height + "px");
        d.css("opacity", $.convertOpacity(a.opacity));
        d.css("background-color", $.convertColor(a.color));
        d.css("border", $.replaceAll(a.border, ":", " "));
        if ("" != a.x) d.css("left", parseInt(a.x));
        if ("" != a.y) d.css("top", parseInt(a.y));
        b.kag.event.addEventElement({
            tag: "clickable",
            j_target: d,
            pm: a
        });
        b.setEvent(d, a);
        c.append(d);
        c.show();
        this.kag.ftag.nextOrder();
    },
    setEvent: function(a, b) {
        var c = TYRANO;
        !function() {
            var d = b.target;
            var e = b.storage;
            var f = b;
            if ("" != f.mouseopacity) {
                a.bind("mouseover", function() {
                    a.css("opacity", $.convertOpacity(f.mouseopacity));
                });
                a.bind("mouseout", function() {
                    a.css("opacity", $.convertOpacity(f.opacity));
                });
            }
            a.click(function() {
                var a = function(a) {
                    if (true != a.kag.stat.is_strong_stop) return false;
                    return true;
                }(c);
                if (false == a) return false;
                c.kag.ftag.startTag("cm", {});
                c.kag.layer.showEventLayer();
                c.kag.ftag.startTag("jump", f);
            });
        }();
    }
};

tyrano.plugin.kag.tag.glyph = {
    pm: {
        line: "nextpage.gif",
        layer: "message0",
        fix: "false",
        left: 0,
        top: 0
    },
    start: function(a) {
        var b = this;
        $(".glyph_image").remove();
        if ("true" == a.fix) {
            var c = this.kag.layer.getLayer(a.layer);
            var d = $("<img class='glyph_image' />");
            d.attr("src", "./tyrano/images/kag/" + a.line);
            d.css("position", "absolute");
            d.css("z-index", 99999);
            d.css("top", a.top + "px");
            d.css("left", a.left + "px");
            d.css("display", "none");
            c.append(d);
            this.kag.stat.flag_glyph = "true";
        } else this.kag.stat.flag_glyph = "false";
        this.kag.ftag.nextOrder();
    }
};

tyrano.plugin.kag.tag.trans = {
    vital: [ "time", "layer" ],
    pm: {
        layer: "base",
        method: "crossfade",
        children: false,
        time: 1500
    },
    start: function(a) {
        this.kag.ftag.hideNextImg();
        this.kag.stat.is_trans = true;
        var b = this;
        var c = 0;
        var d = $.countObj(this.kag.layer.map_layer_fore);
        if ("false" == a.children) d = 0;
        var e = $.cloneObject(this.kag.layer.map_layer_fore);
        var f = $.cloneObject(this.kag.layer.map_layer_back);
        for (key in e) if (true == a.children || key === a.layer) (function() {
            var d = key;
            var g = e[d];
            var h = f[d];
            if (d.indexOf("message") != -1 && "false" == h.attr("l_visible")) {
                c++;
                b.kag.layer.forelay(d);
            } else {
                $.trans(a.method, g, parseInt(a.time), "hide", function() {});
                h.css("display", "none");
                $.trans(a.method, h, parseInt(a.time), "show", function() {
                    c++;
                    b.kag.layer.forelay(d);
                    b.kag.ftag.completeTrans();
                    b.kag.ftag.hideNextImg();
                });
            }
        })();
        this.kag.ftag.nextOrder();
    }
};

tyrano.plugin.kag.tag.bg = {
    vital: [ "storage" ],
    pm: {
        storage: "",
        method: "crossfade",
        wait: "true",
        time: 3e3
    },
    start: function(a) {
        this.kag.ftag.hideNextImg();
        var b = this;
        if (0 == a.time) a.wait = "false";
        var c = "./data/bgimage/" + a.storage;
        if ($.isHTTP(a.storage)) c = a.storage;
        this.kag.preload(c, function() {
            var d = b.kag.layer.getLayer("base", "fore");
            var e = d.clone(false);
            e.css("background-image", "url(" + c + ")");
            e.css("display", "none");
            d.after(e);
            b.kag.ftag.hideNextImg();
            b.kag.layer.updateLayer("base", "fore", e);
            if ("true" == a.wait) b.kag.layer.hideEventLayer();
            $.trans(a.method, d, parseInt(a.time), "hide", function() {
                d.remove();
            });
            $.trans(a.method, e, parseInt(a.time), "show", function() {
                e.css("opacity", 1);
                if ("true" == a.wait) {
                    b.kag.layer.showEventLayer();
                    b.kag.ftag.nextOrder();
                }
            });
        });
        if ("false" == a.wait) this.kag.ftag.nextOrder();
    }
};