[cm]
[clearfix]
[bg storage="room.jpg" time="100"]

;[jump target=*end]

[live2d_new name="renge" jname="ハル" width=1024 height=1024 left=-272 top=200 gltop=0.25  lip=true glscale=1.85 ]

[iscript]
kc.refreshTarget();
kc.loadMotion();
kc.loadExpression();
[endscript]

[live2d_show name="renge" jname="ハル"]

[iscript]
kc.loadParam();
kc.loadPart();
[endscript]

[live2d_expression name="renge" filenm="f03.exp.json"]

[live2d_motion name="renge" filenm="m01.mtn" idle="ON"]


[playbgm storage=music.ogg] 
[wait time=1000]

@jump storage="online.ks"









;[playbgm storage=voice_126.mp3 loop=false] 
;[live2d_expression name="renge" filenm="f03.exp.json"]
[jump target=*end]
#ハル
[mtext text="Text to Display" x=100 y=100 size=50 in_sync out_sync=true in_effect="fadeIn"]
[iscript]
tf.f1 = "f01.exp.json"
[endscript]
[font size=40 bold=true]
This message is big and bold.
[resetfont]
[mtext text="Text to Display" x=100 y=200 size=50 in_effect="fadeIn" out_effect="fadeOut"]
さ[emb exp="tf.f1"]よ[p]
[live2d_motion name="renge" filenm="m01.mtn" ]
[wait time=1000]
[live2d_expression name="renge" filenm=&tf.f1]
[wait time=1000]


表情モーションも再生できるよ[p]
[live2d_expression name="renge" filenm="f03.exp.json"]
怒ったり、[p]
[wait time=1500]
[live2d_expression name="renge" filenm="f04.exp.json"]
困ったり、[p]
[wait time=1500]
[live2d_expression name="renge" filenm="f06.exp.json"]
びっくりしたり、[p]
[wait time=1500]
[live2d_expression name="renge" filenm="f07.exp.json"]
[live2d_expression name="renge" filenm="f03.exp.json"]
怒ったり、
[wait time=1500]
[live2d_expression name="renge" filenm="f04.exp.json"]
困ったり、
[wait time=1500]
[live2d_expression name="renge" filenm="f06.exp.json"]
びっくりしたり、
[wait time=1500]
[live2d_expression name="renge" filenm="f07.exp.json"]
[live2d_expression name="renge" filenm="f03.exp.json"]
怒ったり、
[wait time=1500]
[live2d_expression name="renge" filenm="f04.exp.json"]
困ったり、
[wait time=1500]
[live2d_expression name="renge" filenm="f06.exp.json"]
びっくりしたり、
[wait time=1500]
[live2d_expression name="renge" filenm="f07.exp.json"]
[live2d_expression name="renge" filenm="f03.exp.json"]
怒ったり、
[wait time=1500]
[live2d_expression name="renge" filenm="f04.exp.json"]
困ったり、
[wait time=1500]
[live2d_expression name="renge" filenm="f06.exp.json"]
びっくりしたり、
[wait time=1500]
[live2d_expression name="renge" filenm="f07.exp.json"]
表情豊かなキャラクターを表現できますね♪[p]
[wait time=1500]
;表情をデフォルトに戻す
[live2d_expression name="renge" filenm="f01"]


あとキャラクターの色も変えられるよ[p]
; rengeの色を変える
[live2d_color name="renge" red=0.3 green=0.3 blue=0.3]
[wait time=1500]
[live2d_color name="renge" red=0.6 green=0.3 blue=0.3]
[wait time=1500]
; rengeを元の色に戻す
[live2d_color name="renge" red=1.0 green=1.0 blue=1.0]


友達を紹介するね。イプシロンちゃーんっ！[p]
[live2d_trans name="renge" left=-200 top=0]
[wait time=1000]

#イプシロンちゃん
;[live2d_show name="asu"]
[live2d_trans name="asu" left=160 top=-130]
はいはーい[p]
[wait time=1000]
; アイドルモーションの指定
[live2d_motion name="asu" filenm="asu2.1_m_04.mtn"]

ねっ、これでLive2Dを使ったゲームも作れるね！[p]
[live2d_motion name="renge" filenm="idle_02.mtn"]
[live2d_motion name="asu" filenm="asu2.1_m_sp_01.mtn" idle="ON"]
じゃあ、ゲーム開発を頑張ってね。[r]ばいばーい[p]
[live2d_delete name="renge"]
[live2d_delete name="asu"]

*end