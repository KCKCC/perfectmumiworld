kc.scenario = {
    run: (content = "") => {
        // if (content == "")
        //     content = $("#scenario").val();
        // lines = content.split('\n');
        // lines.forEach((l) => {
        //     arr = TYRANO.kag.parser.parseScenario(l);
        //     TYRANO.kag.ftag.array_tag.push(...arr.array_s)
        // })
        // TYRANO.kag.ftag.current_order_index=0;
        // TYRANO.kag.ftag.nextOrder(); F
        gtag("event", "run_scenario", {
            mumi: "mumi"
        });

        var arr = TYRANO.kag.parser.parseScenario('@jump storage="online.ks"');
        TYRANO.kag.ftag.array_tag.push(...arr.array_s)

        TYRANO.kag.ftag.current_order_index--;
        TYRANO.kag.ftag.nextOrder();

        // don't use cut in scenario Q_Q so annoying
        // TYRANO.kag.rider.cutTyranoScript('@jump storage="online.ks"');
        // TYRANO.kag.stat.current_scenario='online';
    },
    load: (file) => {
        kc.file.load(file.files[0], (x) => { $("#scenario").val(x); });
    },
    loadFromUrl: (url) => {
        if (url == null) {
            url = $("#scenarioFileUrl").val();
        }
        $.ajax({
            url: url + '?' + Math.random(), success: function (x) {
                $("#scenario").val(x);
            }
        });
    },
    save: () => {
        kc.file.save($("#scenario").val(), $("#scenarioName").val() + ".txt");
    },
}

kc.expression = {
    run: () => { },

    save: () => {
        var expression = {
            "type": "Live2D Expression",
            "fade_in": 500,
            "fade_out": 500,
            "params": [
                // { "id": "PARAM_EYE_L_OPEN", "val": 0, "calc": "mult" }
            ]
        }
        Live2Dcanvas[kc.target].live2DModel.expressionRef.motions[0]._$w0.paramList
            .forEach(x => {
                expression.params.push({ id: x.id, val: x.value, type: x.type });
            });
        console.log(expression);
        kc.file.save(JSON.stringify(expression), $("#expressionName").val() + ".json");
    },
    load: (name, content) => {

    },
}

kc.file = {
    save: (content, filename) => {
        saveAs(new Blob([content], { type: "text/plain;charset=utf-8" }), filename);
    },
    load: (file, callback) => {
        var file = document.querySelector('input[type=file]').files[0];
        var reader = new FileReader()

        var textFile = /text.*/;

        if (file.type.match(textFile)) {
            reader.onload = function (event) {
                callback(event.target.result);
            }
        } else {
            callback("姆咪?");
        }
        reader.readAsText(file);

    }
}
kc.soundId = 0;
kc.addSoundBtn = () => {
    var name = kc.soundId++;
    var url = $("#soundUrl").val();
    kc.addSound(name, url);
}
kc.addSound = (name, url) => {
    var pt = $("#soundTemplate").clone();
    pt.attr('id', name);
    $(pt.children()[0].children[0]).val(name);
    $(pt.children()[1].children[0])
        .attr('src', url).attr('crossOrigin', "anonymous");
    pt.appendTo("#soundContainer");
}
kc.playSound = (name, time = 0, loop = false, vol = 0.5) => {
    var tar = $(`#${name}`).find('audio');
    if (tar == undefined) return;
    tar[0].loop = loop;
    tar[0].volume = vol;
    tar[0].currentTime = time;
    tar.get(0).play();
}
kc.stopSound = (name) => {
    $(`#${name}`).find('audio').get(0).pause();
}

kc.stopAllSound = () => {
    $(`#soundContainer`).find('audio').each((k, v) => v.pause());
}

kc.picId = 0;
kc.addPicBtn = () => {
    var name = kc.picId++;
    var url = $("#picUrl").val();
    kc.addPic(name, url);
}
kc.addPic = (name, url) => {
    var pt = $("#picTemplate").clone();
    pt.attr('id', name);
    $(pt.children()[0].children[0]).val(name);
    $(pt.children()[1].children[0])
        .attr('src', url).attr('crossOrigin', "anonymous");
    pt.appendTo("#picContainer");
}
kc.showPicBtn = (self) => {
    var name = $(self).parent().parent().find('input').val();
    var style = $("#picStyle").val();
    kc.showPic(name, style);
}
kc.showPic = (name, style = "transform: translate(0%,0%) scale(0.5);", isBg = false) => {
    style = "position: absolute;z-index: 100; opacity:0;" + style;
    var pt = $("#" + name).find('img').clone();
    pt.attr('class', 'kcp-' + name);
    pt.appendTo(kc.getPicContainer(isBg));
    //to trigger transition
    pt.attr('style', style);
    //tyrano seems expose variable to global... crazy!!

    setTimeout(() => { pt[0].style.opacity = 1; }, 50);
}
kc.hidePic = (name, isBg = false) => {
    $(kc.getPicContainer(isBg)).find('.kcp-' + name).each((k, v) => {
        v.style.opacity = 0;
    });
}
kc.cssPic = (name, style = "transform: translate(0%,0%) scale(0.5);", isBg = false) => {
    $(kc.getPicContainer(isBg)).find('.kcp-' + name).each((k, v) => {
        $(v).attr('style', $(v).attr('style') + style);
    });
}
kc.removePicBtn = (self) => {
    var name = $(self).parent().parent().find('input').val();
    kc.removePic(name);
}
kc.removePic = (name, isBg = false) => {
    $(kc.getPicContainer(isBg)).find('.kcp-' + name).each((k, v) => { v.remove() });
}
kc.cleanPic = (isBg = false) => {
    $(kc.getPicContainer(isBg)).empty();
}
kc.getPicContainer = (isBg) => { return isBg ? "#bgContainer" : "#picStageContainer" };

kc.removeAsset = (x) => {
    $(x).closest("tr").remove();
}

kc.idAsset = (x) => {
    $(x).parent().parent().attr('id', $(x).val());
}