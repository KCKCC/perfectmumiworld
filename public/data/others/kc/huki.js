
kc.hukidasi = { index: 0, type: 'auto', };
kc.hukidasi.createBtn = () => {
    kc.hukidasi.create(
        $("#hukiSerihu").val(),
        $("#hukiParamType").find(':selected').val(),
        $("#hukiParamColor").val(),
        $("#hukiParamX").val(),
        $("#hukiParamY").val(),
        $("#hukiParamInterval").val()
    );
};
kc.colorMap = { "ank": "#ee7298", "as": "#338eff", "ha": "#57d9ff", "hi": "#ffed2d", "kae": "#f99cff", "kad": "#f99cff", "kan": "#f4cb2d", "ko": "#98e6ff", "ku": "#cfff2e", "mik": "#ff494a", "mis": "#bf302c", "mu": "#ffd181", "no": "#ffaa24", "re": "#fc8ff8", "sad": "#87d5b9", "sak": "#efd69b", "si": "#56b1d9", "su": "#54f98e", "ura": "#ff7ca0", "yu": "#ff5a00", "ma": "#ffffff", "it": "#4a5eb6", "hu": "#c33b5e", "av": "#d45996", "ao": "#ceb691", "bo": "#9759a8", "ki": "#af2a43", "ir": "#4c4c64" };

kc.hukidasi.getColor = (name) => {
    var cha = Object.keys(kc.colorMap).find(x => name.includes(x));
    return kc.colorMap[cha];
};
kc.hukidasi.create = (str = '姆咪?!\r給給!\r欸嘿嘿💖', type = "npc1", color = "FF4242", x = '50', y = '80', txtInterval = 63, zindex = 11, cb, fontSize = '30px', fat) => {
    //tyrano trim spaces, so use _ instead Q_<
    str = str.replace(/_/g, ' ');
    str = str.replace(/\\r/g, '\r');

    var faki;

    if (!fat)
        faki = kc.hukidasi.judgeFat(str);
    else
        faki = fat;

    var pt = $("#hukidasiTemplate").clone();
    pt.find(".b-text").addClass(kc.hukidasi.fatClass[faki]);

    pt.find('div')[1].style.backgroundColor = color;
    var hukiId = "huki" + kc.hukidasi.index++;
    pt.attr('class', 'hukidasi b' + kc.hukidasi.fats[faki] + '-' + type).attr('id', hukiId);

    pt.appendTo("#hukidasiContainer");
    if (!isNaN(x))
        x += '%'
    if (!isNaN(y))
        y += '%'

    pt.attr('style', `display:block; opacity:0; z-index: ${zindex};position: fixed;left: ${x};top:${y}; transition: opacity .33s; animation: kc-fadeIn .33s;`);
    pt[0].style.opacity = 1;
    // kc.aaa = pt;
    var txtarea = pt.find(".b-text");

    if (kc.hukidasi.type == "hayai") {
        txtarea.val(str);
        return hukiId;
    }

    txtarea[0].innerHTML = '';
    txtarea.css('font-size', fontSize);
    var arr = [...str];

    var hukisetting = { txtIndex: 0, txtInterval: txtInterval, teachering: false };
    setTimeout(() => { kc.hukidasi.appendTxt(txtarea[0], arr, hukisetting, cb) }, 330);

    // return pt; //return id instead to prevent anything hen in tyrano
    return hukiId;
}
kc.hukidasi.lens = [250, 421];
kc.hukidasi.fats = ['', 'b'];
kc.hukidasi.fatClass = ['', 'faki'];
kc.hukidasi.judgeFat = (str) => {
    var rows = str.split('\r');
    var maxLength = 0;
    rows.forEach(r => {
        $("#b-test")[0].innerText = r;
        if (maxLength < $("#b-test").width())
            maxLength = $("#b-test").width();
    });
    var rate = 3;
    if (rows.length > 1) rate = 1;

    for (var i = 0; i < kc.hukidasi.lens.length - 1; i++) {
        if (maxLength < kc.hukidasi.lens[i] * rate) {
            break;
        }
    };
    // if (i > kc.hukidasi.lens.length - 1)
    //     throw { mumi: function () { m = "too faaaaaat >v<"; alert(m); return m }() };
    return i;
}
kc.hukidasi.appendTxt = (textarea, arr, hukisetting, cb) => {
    var rDelay = 0;
    switch (arr[hukisetting.txtIndex]) {
        case "\r":
            textarea.innerHTML += "<br>"
            rDelay = 200;
            break;
        case "[":
            hukisetting.teachering = true;
            break;
        case "]":
            hukisetting.teachering = false;
            break;
        default:
            if (hukisetting.teachering)
                textarea.innerHTML += `<span class="teacher">${arr[hukisetting.txtIndex]}</span>`;
            else
                textarea.innerHTML += arr[hukisetting.txtIndex];
    }
    // if (arr[hukidasi.txtIndex] == "\r") {
    //     textarea.innerHTML += "<br>"
    // }
    // if (arr[hukidasi.txtIndex] == "[") {
    //     textarea.innerHTML += "<p class='teacher'>"
    // }
    // else
    //     textarea.innerHTML += arr[hukidasi.txtIndex];
    hukisetting.txtIndex++
    if (arr[hukisetting.txtIndex])
        setTimeout(() => { kc.hukidasi.appendTxt(textarea, arr, hukisetting, cb) }, hukisetting.txtInterval * 1 + rDelay);
    else
        if (cb) cb();
}
kc.hukidasi.destory = (hukiId) => {
    var huki = $("#" + hukiId);
    huki[0].style.opacity = 0;
    // pt[0].style.opacity = 0;
    setTimeout(() => { huki.remove(); }, 330);
}
kc.hukidasi.clean = () => {
    $("#hukidasiContainer").empty();
}
kc.hukidasi.getX = (name) => {
    return $(`#Live2D_${name}`)[0].style.left
}
