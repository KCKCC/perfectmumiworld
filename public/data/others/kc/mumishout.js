// An instance of AudioContext
let audioContext = null;

var MEDIA_ELEMENT_NODES = new WeakMap();

// This will become our input MediaStreamSourceNode
let input = null;

// This will become our AnalyserNode
let analyser = null;

// This will become our ScriptProcessorNode
let scriptProcessor = null;

/**
 * Calculate the average value from the supplied array.
 *
 * @param {Array<Int>}
 */
const getAverageVolume = array => {
    const length = array.length;
    let values = 0;
    let i = 0;

    // Loop over the values of the array, and count them
    for (; i < length; i++) {
        values += array[i];
    }

    // Return the avarag
    return values / length;
}

// Wait untill the page has loaded
kc.mumishout = (chaName, paramName, min = 0, max = 1) => {
    // Get the users microphone audio.
    navigator.mediaDevices.getUserMedia({
        audio: true
    }).then(stream => {
        processInput = audioProcessingEvent => {
            // Create a new Uint8Array to store the analyser's frequencyBinCount 
            const tempArray = new Uint8Array(analyser.frequencyBinCount);

            // Get the byte frequency data from our array
            analyser.getByteFrequencyData(tempArray);

            range = max - min;
            raw = getAverageVolume(tempArray);
            norm = raw / 80;
            norm = norm > 1 ? 1 : norm
            val = min + norm * range;

            if (chaName && paramName)
                kc.setParamTy(chaName, paramName, val, 1);
            console.log(val);
        }
        // Create the audio nodes
        audioContext = new AudioContext();
        input = audioContext.createMediaStreamSource(stream);
        analyser = audioContext.createAnalyser();
        scriptProcessor = audioContext.createScriptProcessor();

        analyser.smoothingTimeConstant = 0.3;
        analyser.fftSize = 1024;

        // Connect the audio nodes
        input.connect(analyser);
        analyser.connect(scriptProcessor);
        scriptProcessor.connect(audioContext.destination);

        // Add an event handler
        scriptProcessor.onaudioprocess = processInput;
    }, error => {
        // Something went wrong, or the browser does not support getUserMedia
    });
};

kc.mumisayit = (audio, chas, chaName, paramName, min = 0, max = 1) => {
    audio = $(audio).clone();
    audio = audio.appendTo("#pakuContainer");

    processInput = audioProcessingEvent => {
        // Create a new Uint8Array to store the analyser's frequencyBinCount 
        const tempArray = new Uint8Array(analyser.frequencyBinCount);

        // Get the byte frequency data from our array
        analyser.getByteFrequencyData(tempArray);

        range = max - min;
        raw = getAverageVolume(tempArray);
        norm = raw / 80;
        norm = norm > 1 ? 1 : norm
        val = min + norm * range;
        if (chaName && paramName)
            kc.setParamTy(chaName, paramName, val, 1);
        if (chas) {
            chas.forEach(c => {
                kc.setParamTy(c.chaName, c.paramName, val, 1);
            });
        }
        // console.log(val);
    }

    // Create the audio nodes
    audioContext = new AudioContext();


    input = audioContext.createMediaElementSource(audio[0]);

    analyser = audioContext.createAnalyser();
    scriptProcessor = audioContext.createScriptProcessor();

    analyser.smoothingTimeConstant = 0.3;
    analyser.fftSize = 1024;

    // Connect the audio nodes
    input.connect(analyser);
    analyser.connect(scriptProcessor);
    scriptProcessor.connect(audioContext.destination);
    input.connect(audioContext.destination);
    // Add an event handler
    scriptProcessor.onaudioprocess = processInput;

};

kc.mumidamare = (chaName, paramName, chas, val) => {
    if (chaName && paramName)
        kc.setParamTy(chaName, paramName, val, 1);
    if (chas) {
        chas.forEach(c => {
            kc.setParamTy(c.chaName, c.paramName, val, 1);
        });
    }
    input.disconnect();
    audioContext.close();
    $("#pakuContainer").empty();
}