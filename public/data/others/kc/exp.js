kc.extraExps = {};
kc.addExtraExpression = (name, json) => {
    json.params.forEach(l => {
        switch (l.calc) {
            case "set":
                l.type = 0;
                l.value = l.val
                break
            case "add":
                l.type = 1;
                if (l.def)
                    l.value = l.val - l.def;
                else
                    l.value = l.val
                break
            case "mult":
                l.type = 2;
                if (l.def)
                    l.value = l.val / l.def;
                else
                    l.value = l.val
                break
            default:
                if (l.type == undefined) {
                    l.type = 1;
                    l.value = l.val
                    if (l.def)
                        l.value = l.val - l.def;
                    else
                        l.value = l.val
                } else {
                    l.value = l.val
                }
        }
    });
    kc.extraExps[name] = json;
}
kc.loadExtraExpression = (name, url) => {
    $("#kcExps").append(new Option(name));
    $.getJSON(url, (e) => {
        kc.addExtraExpression(name, e);
    })
};
kc.loadAllExpression = () => {
    var list = ["reset","f01", "achu_f02", "achu_f03", "achu_f04", "achu_f05", "achu_f06", "achu_f07", "achu_f08", "achu_f09", "achu_f10", "achu_f11", "anko_f02", "anko_f03", "anko_f04", "anko_f05", "anko_f06", "anko_f07", "anko_f08", "anko_f09", "anko_f10", "anko_f20", "anko_f21", "anko_f22", "aoi_f02", "aoi_f03", "aoi_f04", "aoi_f05", "aoi_f06", "aoi_f07", "aoi_f08", "aoi_f09", "aoi_f10", "aoi_f11", "aoi_f12", "aoi_f13", "asu_f02", "asu_f03", "asu_f04", "asu_f05", "asu_f06", "asu_f07", "asu_f08", "asu_f09", "asu_f10", "asu_f20", "asu_f21", "asu_f22", "avna_f02", "avna_f03", "avna_f05", "avna_f06", "avna_f07", "avna_f08", "avna_f11", "bota_f02", "bota_f05", "bota_f06", "bota_f07", "bota_f08", "bota_f09", "bota_f12", "eri_f02", "eri_f03", "eri_f04", "eri_f05", "eri_f06", "eri_f07", "eri_f08", "eri_f09", "eri_f10", "eri_f11", "eri_f12", "eri_f13", "eri_f14", "eri_f15", "eri_f16", "eri_f17", "haka_f02", "haka_f03", "haka_f04", "haka_f05", "haka_f06", "haka_f07", "haka_f08", "haka_f09", "haka_f10", "haka_f20", "haka_f21", "haka_f22", "hita_f02", "hita_f03", "hita_f04", "hita_f05", "hita_f06", "hita_f07", "hita_f08", "hita_f09", "hita_f10", "hura_f02", "hura_f03", "hura_f04", "hura_f05", "hura_f06", "hura_f07", "hura_f08", "hura_f09", "hura_f10", "hura_f11", "kade_f02", "kade_f03", "kade_f04", "kade_f05", "kade_f06", "kade_f07", "kade_f08", "kade_f09", "kade_f10", "kano_f02", "kano_f03", "kano_f04", "kano_f05", "kano_f06", "kano_f07", "kano_f08", "kano_f09", "kano_f10", "komi_f02", "komi_f03", "komi_f04", "komi_f05", "komi_f06", "komi_f07", "komi_f08", "komi_f09", "komi_f10", "komi_f20", "komi_f21", "komi_f22", "kumi_f02", "kumi_f03", "kumi_f04", "kumi_f05", "kumi_f06", "kumi_f07", "kumi_f08", "kumi_f09", "kumi_f10", "mari_f02", "mari_f03", "mari_f04", "mari_f05", "mari_f06", "mari_f07", "mari_f08", "mari_f09", "mari_f10", "mari_f11", "mari_f12", "mari_f13", "mari_f14", "miki_f02", "miki_f03", "miki_f04", "miki_f05", "miki_f06", "miki_f07", "miki_f08", "miki_f09", "miki_f10", "miki_f20", "miki_f21", "miki_f22", "mumi_f02", "mumi_f03", "mumi_f04", "mumi_f05", "mumi_f06", "mumi_f07", "mumi_f08", "mumi_f09", "mumi_f10", "nomi_f02", "nomi_f03", "nomi_f04", "nomi_f05", "nomi_f06", "nomi_f07", "nomi_f08", "nomi_f09", "nomi_f10", "ren_f02", "ren_f03", "ren_f04", "ren_f05", "ren_f06", "ren_f07", "ren_f08", "ren_f09", "ren_f10", "ren_f20", "ren_f21", "ren_f22", "saki_f01", "saki_f02", "saki_f03", "saki_f04", "saki_f05", "saki_f06", "saki_f07", "saki_f08", "saki_f09", "saki_f10", "saki_f11", "saki_f12", "saki_f13", "saki_f14", "saki_f15", "saki_f16", "saki_f17", "saku_f02", "saku_f03", "saku_f04", "saku_f05", "saku_f06", "saku_f07", "saku_f08", "saku_f09", "saku_f10", "sane_f02", "sane_f03", "sane_f04", "sane_f05", "sane_f06", "sane_f07", "sane_f08", "sane_f09", "sane_f10", "sane_f11", "siho_f02", "siho_f03", "siho_f04", "siho_f05", "siho_f06", "siho_f07", "siho_f08", "siho_f09", "siho_f10", "siho_f11", "siho_f12", "siho_f13", "siho_f14", "siho_f15", "suba_f03", "suba_f04", "suba_f05", "suba_f06", "suba_f07", "suba_f08", "suba_f09", "suba_f10", "suba_f20", "tuki_f02", "tuki_f03", "tuki_f04", "tuki_f05", "tuki_f06", "tuki_f07", "tuki_f08", "tuki_f09", "tuki_f10", "tuki_f11", "tuki_f12", "ura_f01", "ura_f02", "ura_f03", "ura_f04", "ura_f05", "ura_f06", "ura_f07", "ura_f08", "ura_f09", "ura_f10", "ura_f20", "yuri_f02", "yuri_f03", "yuri_f04", "yuri_f05", "yuri_f06", "yuri_f07", "yuri_f08", "yuri_f09", "yuri_f10", "yuri_f11"];
    list.forEach(i => {
        kc.loadExtraExpression(i, `data/others/kc/exps/${i.replace("_", "/")}.json`);
    });
};
kc.loadExpfromFile = (evt) => {
    var files = evt.target.files; // FileList object
    var name = files[0].name.replace(".json", "");
    // files is a FileList of File objects. List some properties.
    var output = [];
    for (var i = 0, f; f = files[i]; i++) {
        var reader = new FileReader();

        // Closure to capture the file information.
        reader.onload = (function (theFile) {
            return function (e) {
                //console.log('e readAsText = ', e);
                //console.log('e readAsText target = ', e.target);
                try {
                    json = JSON.parse(e.target.result);
                    $("#kcExps").append(new Option(name));
                    //alert('json global var has been set to parsed json of this file here it is unevaled = \n' + JSON.stringify(json));
                    kc.addExtraExpression(name, json);
                } catch (ex) {
                    alert('ex when trying to parse json = ' + ex);
                }
            }
        })(f);
        reader.readAsText(f)
    }
}

kc.expindex = 0;

kc.setExtraExpression = (name, expName) => {
    Live2Dcanvas[name].expressions['mumi' + kc.expindex] = $.extend(true, {}, Live2Dcanvas[name].expressions.mumi);
    //fix damn pollution yo mumi la
    // Live2Dcanvas[name].expressions['mumi' + kc.expindex].paramList = kc.extraExps[expName].params;
    Live2Dcanvas[name].expressions['mumi' + kc.expindex].paramList = $.extend(true, [], kc.extraExps[expName].params);
    Live2Dcanvas[name].expressions['mumi' + kc.expindex]._$dP = kc.extraExps[expName].fade_in;
    Live2Dcanvas[name].expressions['mumi' + kc.expindex]._$eo = kc.extraExps[expName].fade_out;
    Live2Dcanvas[name].expressionChange(name, 'mumi' + kc.expindex);
    kc.expindex++;
    if (kc.expindex > 2) kc.expindex = 0;
};
kc.setExtraExpressionBtn = (sel) => {
    if (!kc.checkTarget()) return;
    kc.setExtraExpression(kc.target, sel.value);
}

kc.randomParam = (excludes) => {
    if (!kc.checkTarget()) return;
    $("#paramContainer").find('tr').each((k, v) => {
        if (excludes && excludes.some(i => v.children[0].innerText.includes(i)))
            return;
        v = $(v).find('input')[0];
        var val = (v.max - v.min) * Math.random() + parseFloat(v.min);
        if (val > v.max) val = v.max;
        if (val < v.min) val = v.min;
        v.value = val;
        v.oninput();
    })
    if ($("#keepChanging")[0].checked)
        setTimeout(() => kc.randomParam(excludes), $("#randomInterval").val());
}
//type 0 set 1 add 2 mult
kc.randomParamTy = (name, excludes, type = 1) => {

    // var tempParamList = Live2Dcanvas[name].live2DModel._$MT._$vo._$4S.map(x => x._$wL.id);
    $(Live2Dcanvas[name].live2DModel._$MT._$vo._$4S)
        .each((k, x) => {
            if (excludes && excludes.some(i => x._$wL.id.includes(i)))
                return;
            var ranValue = (x._$LT - x._$TT) * Math.random() + parseFloat(x._$TT);
            if (ranValue > x._$LT) ranValue = x._$LT;
            if (ranValue < x._$TT) ranValue = x._$TT;
            kc.setParamTy(name, x._$wL.id, ranValue, type)
        });
}


