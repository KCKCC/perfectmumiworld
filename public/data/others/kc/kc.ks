[macro name = "kc_dah"]
    [wait time=0]
[endmacro]

[macro name = "kc_prepareL2D"]
    [iscript]
        kc.prepareModelTy(mp.name, mp.url, mp.modelFileName)
    [endscript]
[endmacro]

[macro name = "kc_transDuration"]
    [iscript]
        kc.setL2DTransDuration(mp.name, mp.time)
    [endscript]
[endmacro]

;works on mumi expression, so u can setparam first than change to mumi or change to mumi first to gain extra effect
[macro name = "kc_L2DsetParam"]

    [iscript]
    ; optinal
    if(mp.name ==null)console.error('nameは必須です');
    if(mp.paramName == null)console.error('paramNameは必須です');
    if(mp.value == null)console.error('valueは必須です');
    if(mp.type == null)mp.type = 1;

    ;TG.stat.is_strong_stop = true;
    ;var complete_event = function(){
    ;	TG.stat.is_strong_stop = false;
    ;	TG.layer.showEventLayer();
    ;	TG.ftag.nextOrder();
    ;};


    ; Live2Dモデルの表示[Live2Dtyrano.js]
    ;kc.cloneToMumi(mp.name);
    kc.setParamTy(mp.name, mp.paramName,mp.value,mp.type);
    [endscript]
[endmacro]

[macro name = "kc_setPart"]
    [iscript]
        if(mp.name ==null)console.error('nameは必須です');
        if(mp.name ==null)console.error('partNameは必須です');
        if(mp.name ==null)console.error('valは必須です');
        kc.setPartTy(mp.name,mp.partName,mp.val);
    [endscript]
[endmacro]

[macro name = "kc_position"]
    [iscript]
        if(mp.name ==null)console.error('nameは必須です');
        kc.positionTy(mp.name, mp.x, mp.y, mp.r, mp.xs, mp.ys);
    [endscript]
[endmacro]

[macro name = "kc_loadAllExp"]
    [iscript]
        kc.loadAllExpression()
    [endscript]
[endmacro]

[macro name = "kc_loadExp"]
    [iscript]
        if(mp.name ==null)console.error('nameは必須です');
        if(mp.url ==null)console.error('urlは必須です');
        kc.loadExtraExpression(mp.name,mp.url)
    [endscript]
[endmacro]

[macro name = "kc_exp"]
    [iscript]
        if(mp.name ==null)console.error('nameは必須です');
        if(mp.expName ==null)console.error('expNameは必須です');
        kc.setupMumi(mp.name);
        kc.setExtraExpression(mp.name, mp.expName);
        if(mp.time==null) 
            tf.flag1 =1000;
        else
            tf.flag1 =mp.time;
    [endscript]
    [wait time=&tf.flag1 ]
[endmacro]

[macro name = "kc_randomParam"]
    [iscript]
        if(mp.name ==null)console.error('nameは必須です');
        if(mp.excludes !=null)mp.excludes=mp.excludes.split(',')
        kc.randomParamTy(mp.name, mp.excludes, mp.type);
    [endscript]
[endmacro]

;------------------------------------------------------------------------hukidasi
;kc.hukidasi.create = (str = '姆咪?!\r給給!\r欸嘿嘿💖', type = "npc1", color, x = '32', y = '72', txtInterval = 63)
[macro name = "kc_hukidasi"]

    [iscript]
        if(mp.str ==null)console.error('strは必須じゃないですが。。。');
        if(mp.type == null)console.error('typeも必須ではないすね。。。');
        if(mp.endWait==null) mp.endWait=800;
        TG.stat.is_strong_stop = true;
        var complete_event = function(){
            TG.stat.is_strong_stop = false;
            TG.layer.showEventLayer();
            TG.ftag.nextOrder();
        };
        if(mp.name && mp.paku){
            mumi=kc.mumiPaku(mp.name);
        }
        if (!mp.color && mp.name) {
            mp.color = kc.hukidasi.getColor(mp.name);
        }
        if (!mp.x && mp.name) {
            mp.x=kc.hukidasi.getX(mp.name);
        }
        hkdsId = kc.hukidasi.create(mp.str, mp.type, mp.color, mp.x, mp.y, mp.txtInterval,mp.zindex,complete_event,mp.fontSize,mp.fat)
    [endscript]

    [if exp="kc.hukidasi.type!='auto'"] 
        [l]
    [else]
        [wait time=&mp.endWait]
    [endif] ;

    [iscript]
        kc.hukidasi.destory(hkdsId);
        if(mp.name && mp.paku)
            kc.shutupmumi(mumi);
    [endscript]

    [wait time=500]

[endmacro]

;------------------------------------------------------------------------sound
[macro name = "kc_addSound"]
    [iscript]
        if(mp.name == null || mp.url == null )
            console.error('name to urlは必須です唷!! ');
        else
            kc.addSound(mp.name, mp.url)
    [endscript]
[endmacro]

[macro name = "kc_playSound"]
    [iscript]
        if(mp.name == null)
            console.error('nameは必須です唷!! ');
        else
            kc.playSound(mp.name, mp.time, mp.loop, mp.vol)
    [endscript]
[endmacro]

[macro name = "kc_stopSound"]
    [iscript]
        if(mp.name == null )
            kc.stopAllSound()
        else
            kc.stopSound(mp.name)
    [endscript]
[endmacro]

[macro name = "kc_sayit"]
    [iscript]
        if(mp.name ==null)console.error('nameは必須です');
        if(mp.sound ==null)console.error('soundは必須です');
        if(!mp.delay)mp.delay=0;
        setTimeout(()=>{
            kc.mumisayit($("#"+mp.sound).find('audio')[0], null, mp.name, "PARAM_MOUTH_OPEN_Y", min = 0, max = 1)
            if(mp.startTime !=null)$("#pakuContainer").find('audio')[0].currentTime=mp.startTime
            $("#pakuContainer").find('audio')[0].play();
            checkSaid = (audio) => {
                if (audio.paused)
                    kc.mumidamare(mp.name, "PARAM_MOUTH_OPEN_Y",null,0);
                else
                    setTimeout(() => checkSaid(audio), 50)
            }
            checkSaid($("#pakuContainer").find('audio')[0]);
        },mp.delay);
    [endscript]
[endmacro]
;------------------------------------------------------------------------pic
[macro name = "kc_addPic"]
    [iscript]
        if(mp.name == null || mp.url == null )
            console.error('name to urlは必須です娃!! ');
        else
            kc.addPic (mp.name, mp.url);
    [endscript]
[endmacro]

[macro name = "kc_showPic"]
    [iscript]
        if(mp.name == null)
            console.error('name は必須です娃yo!! ');
        else
            kc.showPic(mp.name, mp.style, mp.isBg);
    [endscript]
[endmacro]

[macro name = "kc_hidePic"]
    [iscript]
        if(mp.name == null)
            console.error('name は必須です娃yo!! ');
        else
            kc.hidePic(mp.name, mp.isBg);
    [endscript]
[endmacro]

[macro name = "kc_removePic"]
    [iscript]
        if(mp.name == null)
            console.error('name は必須です娃yo哈!! ');
        else
            kc.removePic(mp.name, mp.isBg);
    [endscript]
[endmacro]

[macro name = "kc_piclean"]
    [iscript]
        kc.cleanPic(mp.isBg);
    [endscript]
[endmacro]

[macro name = "kc_cssPic"]
    [iscript]
        if(mp.name == null || mp.style == null )
            console.error('name to styleは必須です娃yo哈辣!! ');
        else
            kc.cssPic (mp.name, mp.style, mp.isBg);
    [endscript]
[endmacro]

[macro name = "kc_mumiPic"]
    [iscript]
        if(mp.name == null || mp.time == null )
            console.error('name to time は必須です娃yo哈辣逆姆咪ぃぃ!!');
        else
            kc.showPic(mp.name, "transform: translate(-50%,-50%);left: 50%;top: 50%;width: 50%; transition-duration:500ms; z-index:15");
    [endscript]
    [wait time=&mp.time]
    [iscript]
        if(mp.name == null || mp.time == null )
            console.error('name to time は必須です娃yo哈辣逆姆咪ぃぃ哦@v@!!');
        else
            kc.hidePic(mp.name);
    [endscript]
    [wait time=500]
    [iscript]
        kc.removePic(mp.name);
    [endscript]
[endmacro]