kc.isFillHeight = true;
kc.fitphone = () => {
    kc.isFillHeight = !kc.isFillHeight;
    var view_width = $.getViewPort().width;
    var view_height = $.getViewPort().height;

    var width_f = view_width / TYRANO.kag.config.scWidth;
    var height_f = view_height / TYRANO.kag.config.scHeight;
    if (kc.isFillHeight)
        $(".tyrano_base").css("transform", "scale(" + height_f + ") ");
    else
        $(".tyrano_base").css("transform", "scale(" + width_f + ") ");
}
kc.setWH = () => {
    localStorage.setItem('kcw', $("#kcw").val());
    localStorage.setItem('kch', $("#kch").val());
}
kc.getWH = () => {
    $("#kcw").val(localStorage.getItem('kcw'));
    $("#kch").val(localStorage.getItem('kch'));
}
kc.fullscreen = () => {
    $("#tywrap")[0].requestFullscreen()
}

kc.snapOK = false;
kc.prepareVideo =
    async function () {
        var video = document.querySelector('#picSaveVideo');
        const displayMediaOptions = {
            video: {
                cursor: "never"
            },
            audio: false
        }

        video.srcObject = await navigator.mediaDevices.getDisplayMedia(displayMediaOptions);
    }
kc.capture =
    async function () {
        kc.show('#kc-menu');
        if (!kc.snapOK) {
            await kc.prepareVideo();
            kc.snapOK = true;
        }
        setTimeout(() => {
            var canvas = document.getElementById('picSaveCanvas');
            var video = document.getElementById('picSaveVideo');
            canvas.width = video.videoWidth;
            canvas.height = video.videoHeight;
            canvas.getContext('2d').drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
            canvas.toBlob(function (blob) {
                saveAs(blob, "mumi.png");
            });
        }, 500)

    }
kc.snapL2d =
    function () {
        if (!kc.checkTarget()) return;
        $('.layer_event_click').css('z-index', 0)
        alert('right click canvas and save it >v@')
    }
kc.allTypeSwitch = (sel) => {
    $('#paramContainer').find('select').each((k,v) => v.value =  sel.value);
}