kc = {
    x: 0, y: 0, r: 0, s: 1,
    isNotMumi: true,
    target: null,
    changingExp: false,
    tabState: 1,
    tabs: ["#KC-main", "#kc-scenario"],
    autoMap: { name: { temp: "{name}", val: function () { return kc.target; } } },
    font: (lang) => { $('.b-text').css('font-family', lang) },
    show: (id) => {
        if ($(id).is(":visible")) $(id).hide()
        else $(id).show();
    },
    setL2DTransDuration : (name, time) => { $(`#Live2D_${name}`).css('transition-duration', time + "ms"); },
    position: (type, sel, name) => {
        if (!kc.checkTarget()) return;
        if (name == undefined) name = kc.target;
        switch (type) {
            case "x":
                kc.x = sel.value;
                $(`#Live2D_${name}`).css('left', kc.x + '%');
                break;
            case "y":
                kc.y = sel.value;
                $(`#Live2D_${name}`).css('top', kc.y + '%');
                break;
            case "r":
                kc.r = sel.value;
                break;
            case "s":
                kc.s = sel.value;
                break;
        }
        $(`#Live2D_${name}`).css('transform', `translate(-50%, -50%) rotate(${kc.r}deg) scale(${kc.s}, ${kc.s})`);
    },
    positionReset: () => {
        if (!kc.checkTarget()) return;
        var inputs = $("#modelManu").find('input');
        inputs['0'].value = 50;
        kc.x = 50;
        inputs['1'].value = 55;
        kc.y = 55;
        inputs['2'].value = 0;
        kc.r = 0;
        inputs['3'].value = 1;
        kc.s = 1;
        kc.positionTy(kc.target, 50, 55, 0, 1);
    },
    positionTy: (name, x, y, r = 0, xs = 1, ys = 1) => {
        if (name == undefined) name = kc.target;
        // x = x ? x : 0; y = y ? y : 0; r = r ? r : 0; xs = xs ? xs : 1; ys = ys ? ys : 1;
        if (x)
            $(`#Live2D_${name}`).css('left', x + '%');
        if (y)
            $(`#Live2D_${name}`).css('top', y + '%');
        $(`#Live2D_${name}`).css('transform', `translate(-50%, -50%) rotate(${r}deg) scale(${xs}, ${ys})`);
    },
    checkTarget: () => {
        if (kc.target == null) {
            throw { mumi: function () { m = "空～空~姆咪~💛"; alert(m); return m }() };
        }
        return true;
    },
    refreshTarget: () => {
        $('#target')
            .find('option')
            .remove();
        Object.keys(Live2Dcanvas).forEach(x => $("#target").append(new Option(x)));
        kc.target = kc.target == null ? $("#target")[0].value : kc.target;
        kc.changeTarget(kc.target);
    },
    changeTarget: (val) => {
        kc.target = val;
        kc.loadMotion();
        kc.loadExpression();
        kc.loadParam();
        kc.loadPart();
    },
    loadMotion: () => {
        $('#motionlist')
            .find('option')
            .remove();
        if (!kc.checkTarget()) return;

        new Set(Live2Dcanvas[kc.target].motionfilesnm)
            .forEach(x => $("#motionlist").append(new Option(x)));
    },
    loadExpression: () => {
        $('#expressionlist')
            .find('option')
            .remove();
        if (!kc.checkTarget()) return;

        new Set(Object.keys(Live2Dcanvas[kc.target].expressions))
            .forEach(x => $("#expressionlist").append(new Option(x)));
    },
    loadParam: () => {
        $('#paramContainer').children().remove();
        if (!kc.checkTarget()) return;
        Live2Dcanvas[kc.target].live2DModel._$MT._$vo._$4S
            .forEach(x => {
                // $("#paramlist").append(new Option(x._$wL.id, `${x._$TT},${x._$LT}`));
                pt = $("#paramTemplate").clone().attr('id', `param_${x._$wL.id}`);
                pt.children()[0].textContent = x._$wL.id;
                pt.children()[2].textContent = x._$TT;
                $(pt.children()[3].children[0])
                    .attr('onInput', `kc.setParam(this,'${x._$wL.id}')`)
                    .attr('value', x._$FS)
                    .attr('min', x._$TT).attr('max', x._$LT).attr('step', (x._$LT - x._$TT) / 100);
                pt.children()[4].textContent = x._$LT;
                pt.appendTo("#paramContainer");
            });
    },
    loadPart: () => {
        $('#partContainer').children().remove();
        if (!kc.checkTarget()) return;
        Live2Dcanvas[kc.target].live2DModel._$5S._$Hr
            .forEach(x => {
                // $("#paramlist").append(new Option(x._$wL.id, `${x._$TT},${x._$LT}`));
                pt = $("#partTemplate").clone();
                pt.children()[0].textContent = x._$e0._$NL.id;
                $(pt.children()[1].children[0])
                    .attr('onInput', `kc.setPart(this,'${x._$e0._$NL.id}')`)
                    .attr('value', 1);
                pt.appendTo("#partContainer");
            });

    },
    setMotion: () => {
        if (!kc.checkTarget()) return;
        var loop = $("#motionloop")[0].checked ? 'idle="ON"' : "";
        TYRANO.kag.rider.cutTyranoScript(`[live2d_motion name="${kc.target}" filenm="${$("#motionlist")[0].value}" ${loop}]`);
    },
    setExpression: (sel) => {
        if (!kc.checkTarget()) return;
        kc.changingExp = true;
        TYRANO.kag.rider.cutTyranoScript(`[live2d_expression name="${kc.target}" filenm="${sel.value}"]`);
        setTimeout((() => {
            kc.changingExp = false;
        }), 500);
        kc.isNotMumi = true;
    },
    cloneToMumi: (name) => {
        var newMumi = $.extend(true, {}, Live2Dcanvas[name].live2DModel.expressionRef.motions[0]._$w0);
        Live2Dcanvas[name].expressions.mumi = newMumi;
        // TYRANO.kag.rider.cutTyranoScript(`[live2d_expression name="${kc.target}" filenm="mumi"]`);
        Live2Dcanvas[name].expressionChange(name, 'mumi');
    },
    setParam: (sel, paramName) => {
        if (!kc.checkTarget()) return;
        if (Live2Dcanvas[kc.target].live2DModel.expressionRef.motions[0]._$w0 != Live2Dcanvas[kc.target].expressions.mumi) {
            kc.cloneToMumi(kc.target);
        }
        var type = $(sel).parent().parent().find(':selected').val();
        kc.setParamTy(kc.target, paramName, sel.value, type);
    },
    getParamTy: (name, paramName) => {
        var currentExp = Live2Dcanvas[name].live2DModel.expressionRef.motions[0]._$w0;
        var existedParam = currentExp.paramList
            .find(x => x.id === paramName);
        if (existedParam)
            return Number(existedParam.value);
        return 0;
    },
    setParamTy: (name, paramName = "PARAM_MOUTH_OPEN_Y", value = 0, type = 1) => {
        //console.log("set");
        var currentExp = Live2Dcanvas[name].live2DModel.expressionRef.motions[0]._$w0;
        var existedParam = currentExp.paramList
            .find(x => x.id === paramName);
        if (existedParam) {
            existedParam.value = value;
            existedParam.type = type;
        }
        else {
            var param = new L2DExpressionParam();
            param.id = paramName;
            param.type = type;
            param.value = value;
            currentExp.paramList.push(param);
        }

    },
    setPart: (sel, partName) => {
        if (!kc.checkTarget()) return;
        kc.setPartTy(kc.target, partName, sel.value);
    },
    setPartTy: (name, partName, value) => {
        Live2Dcanvas[name].live2DModel._$5S._$Hr.find(x => x._$e0._$NL.id == partName)._$VS = value;
    },
    loadModel: (name, url, json) => {
        if (!name) { name = $("#modelName").val() }
        if (!url) { url = $("#modelUrl")[0].value }
        if (!json) { json = $("#modelJson")[0].value }
        if (name == "" || url == "") { alert("muni?"); return; }
        LIVE2D_MODEL[name] = {
            "filepath": url,
            "modeljson": json
        };
        TYRANO.kag.rider.cutTyranoScript(`[live2d_new name="${name}" width=1024 height=1024 left=50% top=55% gltop=0  lip=true glscale=1.85 ]`)
        setTimeout((() => {
            TYRANO.kag.rider.cutTyranoScript(`[live2d_show name="${name}"]`);
            kc.refreshTarget();
            kc.setupMumi(name);
            //what's this for QQ?
            //kc.setParamTy(name, "mumi", 0, 0);
            kc.changeTarget($("#target").val());

        }), $("#wait").val());
    },
    setupMumi: (name) => {
        var exps = Live2Dcanvas[name].expressions;
        var newMumi = $.extend(true, {}, exps[Object.keys(exps)[0]]);
        exps.mumi = newMumi;
    },
    prepareModelTy: (name, url, modelFileNam = "model.json") => {
        LIVE2D_MODEL[name] = {
            "filepath": url,
            "modeljson": modelFileNam
        };
    },
    showModel: (name) => {
        if (!kc.checkTarget()) return;
        if (!name) { name = kc.target }
        TYRANO.kag.rider.cutTyranoScript(`[live2d_show name="${name}"]`);
        kc.position();
    },
    hideModel: (name, time = 200) => {
        if (!kc.checkTarget()) return;
        if (!name) { name = kc.target }
        TYRANO.kag.rider.cutTyranoScript(`[live2d_opacity name="${name}" opacity=0 time=${time}]`);
        setTimeout(() => { TYRANO.kag.rider.cutTyranoScript(`[live2d_hide name="${name}"]`) }, time);
    },
    runTy: () => {
        TYRANO.kag.rider.cutTyranoScript($("#tyScript")[0].value);
    },
    cleanTy: () => { $("#tyScript").val('') },
    addTy: () => { $("#scenario")[0].value += $("#tyScript").val() + "\r\n" },
    mumiPaku: (name, paramName = "PARAM_MOUTH_OPEN_Y", min = 0, max = 0.8, time = 463, interval = 50, type = 0) => {

        var mumi = { index: 0, speaking: true, shutupmumi: 0 };
        x = Live2Dcanvas[name].live2DModel._$MT._$vo._$4S.find(x => x._$wL.id == paramName);
        var range = x._$LT - x._$TT;
        min = range * min;
        max = range * max;
        range = max - min;
        var frameCount = Math.floor((time / interval) / 2);
        var dt = range / frameCount;
        var steps = [];
        for (i = 0; i < frameCount; i++) {
            steps.push(min + dt * i);
        }
        for (i = 0; i < frameCount; i++) {
            steps.push(max - dt * i);
        }
        var shutupmumi = setInterval((paramName, steps, mumi) => {
            // Live2Dcanvas[name].live2DModel.setParamFloat(paramName, steps[mumi.index], 1);
            kc.setParamTy(name, paramName, steps[mumi.index], type);
            if (++mumi.index > steps.length - 1) mumi.index = 0;
            if (!mumi.speaking) {
                kc.setParamTy(name, paramName, steps[0], type);
                clearInterval(mumi.shutupmumi);
            }
        }, interval, paramName, steps, mumi);
        mumi.shutupmumi = shutupmumi;
        return mumi;
    },
    shutupmumi: (mumi) => {
        mumi.speaking = false;
    },
    log: () => {
        var data = `{"url":"${document.referrer}"}`;
        $.ajax({
            type: 'post',
            url: "https://script.google.com/macros/s/AKfycbwRRldsnuQ0V8WOa-TLEAq8Wq7I_ngxkMsfg3gqZwfkjKp3v_U/exec",
            data: data
        })
    }
}