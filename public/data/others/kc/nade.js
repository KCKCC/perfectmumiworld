kc.nadeDo = 0;
kc.nadeKando = 100;
kc.nadeSet = (char, nadeReaction = "mumi", x = 100, y = 100, width = 400, height = 300) => {
    $("body").append('<div id="nadeZone"></div>');
    var nadeZone = $("#nadeZone");
    nadeZone.css({ "left": x + 'px', "top": y + 'px', "width": width + 'px', "height": height + 'px' });
    $("body").append('<div id="otete"></div>');
    var otete = $("#otete");
    nadeZone.on("mousedown", function (e) {
        if (nadeReaction.down)
            nadeReaction.down();
        e.preventDefault();

    }).on("mouseup", function () {
        $(this).removeClass("mouseDown");
        if (nadeReaction.up)
            nadeReaction.up();
        otete.hide();

    }).on("mousemove", function (e) {
        if (e.buttons == 1) {
            if (nadeReaction.move)
                nadeReaction.move();
            $(this).addClass("mouseDown");
            otete[0].style.top = e.clientY + 'px';
            otete[0].style.left = e.clientX + 'px';
            otete.show();
            kc.nadeCount++;
            kc.nadeReact(char, e.originalEvent.movementX, e.originalEvent.movementY);
        }
    }).on("mouseout", function (e) {
        if (nadeReaction.out)
            nadeReaction.out();
        $(this).removeClass("mouseDown");
        otete.hide();
    })
};
kc.nadeReact = (char, dX, dY) => {
    kc.nadeDo += ((Math.abs(dX) + Math.abs(dY)) * kc.nadeKando) / 1000;
    var val = kc.getParamTy(char, "PARAM_ANGLE_Z");
    val = val + (dX * kc.nadeKando / 1000);
    kc.setParamTy(char, "PARAM_ANGLE_Z", kc.nadeBound(val, 30, -30), 1);

    val = kc.getParamTy(char, "PARAM_ANGLE_Y");
    val = val + (-dY * kc.nadeKando / 1000);
    kc.setParamTy(char, "PARAM_ANGLE_Y", kc.nadeBound(val, 30, -30), 1);
    setTimeout(() => {
        val = kc.getParamTy(char, "PARAM_UPPER_BODY");
        val = val + (-dY * kc.nadeKando / 20000);
        kc.setParamTy(char, "PARAM_UPPER_BODY", kc.nadeBound(val, 1, -1), 1);

        val = kc.getParamTy(char, "PARAM_ROTATION_Z");
        val = val + (dX * kc.nadeKando / 40000);
        kc.setParamTy(char, "PARAM_ROTATION_Z", kc.nadeBound(val, 1, -1), 1);
    }, 100)
}
kc.nadeBound = (val, max, min) => {
    val = val > max ? max : val;
    val = val < min ? min : val;
    val = val == 0 ? 0.0001 : val;
    return val;
}
kc.nadeClear = () => {
    kc.nadeDo = 0;
    $("#nadeZone").remove();
    setTimeout(() => {
        $("#otete").hide();
    }, 50);

}