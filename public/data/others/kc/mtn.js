kc.allMotion = {};
kc.loadAllMotion = () => {
    list = [];
    list.forEach(i => {
        $.getJSON(`data/others/kc/mtns/${i}.mtn`, (e) => {
            //console.log(e);
            e.params.forEach(l => {
                switch (l.calc) {
                    case "set":
                        l.type = 0;
                        l.value = l.val
                        break
                    case "add":
                        l.type = 1;
                        if (l.def)
                            l.value = l.val - l.def;
                        else
                            l.value = l.val
                        break
                    case "mult":
                        l.type = 2;
                        if (l.def)
                            l.value = l.val / l.def;
                        else
                            l.value = l.val
                        break
                    default:
                        l.type = 1;
                        l.value = l.val
                }
            });
            kc.allMotion[`${i.replace("/", "_")}`] = e;
        })
    });
};
kc.loadAllMotion();
kc.expindex = 0;
kc.setKCMotion = (name, expName) => {
    Live2Dcanvas[name].Motions['mumi' + kc.expindex] = $.extend(true, {}, Live2Dcanvas[name].Motions.mumi);
    Live2Dcanvas[name].Motions['mumi' + kc.expindex].paramList = kc.allMotion[expName].params;
    Live2Dcanvas[name].MotionChange(name, 'mumi' + kc.expindex);
    kc.expindex++;
    if (kc.expindex > 2) kc.expindex = 0;
};